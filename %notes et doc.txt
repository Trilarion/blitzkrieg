
! Beaucoup plus simple qu'avoir un fil séparé pour gérer les animations : le panneau gère une
  liste d'animations courantes avec un chronomètre qui vient rappeler à intervalles réguliers
  qu'il faut mettre à jour le dessin (tant que l'animation est en cours)
    => Tout se fait dans le fil de dessin, pas de problèmes de synchronisation
    => Pas besoin d'identifier les animations, il suffit de toutes les consulter quand on fait le
       dessin (en général il n'y en aura qu'une, ça ne sera pas si violent)
        ! Ce serait bien quand même de garder l'image peinte et de modifier juste la case, pour
          éviter de goinfrer le processeur à "jouer" l'animation (enfin surtout pour des
          machines lentes) 
        ! S'il y a plusieurs animations en même temps, il faudrait qu'elles utilisent un
          même chronomètre, la seconde arrivée réinitialisant le temps pendant lequel il
          va fonctionner ; ça synchroniserait les animations, ce qui éviterait qu'on ait
          deux fois plus d'affichage à faire (à chaque fois qu'une image change dans une des
          animations : si c'est au même moment et qu'on n'a qu'un seul signal du chronomètre
          on n'augmente pas le nombre de réaffichages quand on a plusieurs animations en cours)
            ! Ce ne serait sans doute pas beaucoup plus compliqué avec ça
     ! Doc sur la méthode employée et les éventuelles idées alternatives

! D'après les règles de Neopendulous (dans la page html), les attaques navales ont toujours
  une force de 2. Ca me paraît pas terrible : si une côte est un peu défendu, il n'y a pas
  moyen de la prendre (en particulier s'il y a une seule vile qui est le port, il est très
  facile à défendre, le bonus d'attaque n'étant que de +25% à chaque nouvel assaut)
    => En fait on peut aussi débarquer à côte du port (un peu plus loin) et encercler
       l'ennemi, port compris ; le nombre maximal de troupes aide aussi à empêcher que
       l'ennemi concentre toutes ces troupes à un endroit et c'est tout ; et si l'ennemi
       s'est retanché sur une tout petite île, il ne gagnera pas tellement de points
       (pour les scénarios, il faudra y faire attention : à noter)

! Quand on attaque une case non découverte (par mer), elle devient visible (sans ses voisines)
    => On a un peu exploré la case et ça permet de voir ce qui nous y attend (alors
       qu'actuellement on n'a aucune information sur la case si elle n'est pas découverte, même si elle
       est attaquable ; à modifier ?) ; ça évite par exemple d'attaquer comme un idiot une case bien
       défendue, ou même une ville
    ! Je ne crois pas que c'était comme dans dans pendulous (pas grave, mais à noter quelque part dans
      les règles)
