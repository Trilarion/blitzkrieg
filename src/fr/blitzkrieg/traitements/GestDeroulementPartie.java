/**
 * 
 * Copyright 2009-2010 Sylvain Lavalley
 * 
 * This file is part of Blitzkrieg.
 *
 * Blitzkrieg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Blitzkrieg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blitzkrieg. If not, see <http://www.gnu.org/licenses/>.
 *  
 */
package fr.blitzkrieg.traitements;

import fr.blitzkrieg.donnees.Joueur ;
import fr.blitzkrieg.donnees.Partie ;
import fr.blitzkrieg.traitements.ia.IA ;
import fr.blitzkrieg.traitements.ia.IASimpleAnalyse ;
import fr.blitzkrieg.traitements.ia.IAStandard ;
import fr.blitzkrieg.traitements.ia.JoueurIA ;



// Composant chargé du déroulement de la partie
public class GestDeroulementPartie
{

    private Partie partie ;                 // Partie à gérer
    
    
    
    // Constructeur
    public GestDeroulementPartie (Partie partie)
    {
        this.partie = partie ;
    }
    
    
    // Renvoie la partie gérée
    public Partie partie ()
    {
        return this.partie ;
    }
    
    
    // Lance la partie
    // NOTE Cette méthode ne devrait être appelée que par la classe principale de l'application
    public void commencerPartie ()
    {
        // Créer les joueurs IA
        for (Joueur joueur : this.partie.joueurs())
        {
            // NOTE Les joueurs inactifs sont aussi créés
            //      (sinon il faut les détruire dès qu'il sont inactifs pour garder la cohérence)
            if (! joueur.estHumain())
            {
//                IA ia = new IAPrimitive (joueur, this.partie) ;
//                IA ia = new IASimple    (joueur, this.partie) ;
//                IA ia = new IASimpleAnalyse (joueur, this.partie) ;
                IA ia = new IAStandard (joueur, this.partie) ;
                Thread filJoueurIA = new Thread (new JoueurIA (joueur, this.partie, ia)) ;
                filJoueurIA.setDaemon (true) ;
                filJoueurIA.start() ;
            }
        }
        
        // Passer au premier joueur
        this.passerAuJoueurSuivant() ;

        // Commencer la partie
        this.partie.commencer() ;
    }
    
    
    // Arrête la partie
    // NOTE Cette méthode ne devrait être appelée que par la classe principale de l'application
    // NOTE Les joueurs IA devraient s'arrêter seuls une fois la partie terminée
    public void terminerPartie ()
    {
        this.partie.terminer() ;
    }
    
    
    // Passe au joueur suivant
    // S'il n'y a plus de joueurs actifs, il n'y a plus de joueur courant
    // Renvoie vrai si on a atteint la fin de la liste des joueurs ou s'il n'y a plus de joueurs actifs
    //   (si un tour ou la partie s'est terminé)
    public boolean passerAuJoueurSuivant ()
    {
        Regles regles = this.partie.regles() ;          // Règles de la partie courante
        
        
        // Cas où il n'y a plus de joueurs actifs
        if (! this.partie.resteDesJoueurs())
        {
            this.partie.terminer() ;
            return true ;
        }

        // Terminer le tour du joueur en cours
        // TODO Eliminer autrement le cas du premier tour ?
        Joueur joueurCourant = this.partie.joueurCourant() ;
        if (joueurCourant != null)
            regles.finTourJoueur (joueurCourant) ;
        
        // Passer au joueur suivant
        boolean finTourAtteinte = this.partie.passerAuJoueurSuivant() ;
        
        // Commencer le tour du nouveau joueur
        if (this.partie.enCours())
            regles.debutTourJoueur (this.partie.joueurCourant()) ;
        
        // Indiquer si on a terminé un tour de jeu
        return finTourAtteinte ;
    }

    
}
