/**
 * 
 * Copyright 2009-2010 Sylvain Lavalley
 * 
 * This file is part of Blitzkrieg.
 *
 * Blitzkrieg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Blitzkrieg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blitzkrieg. If not, see <http://www.gnu.org/licenses/>.
 *  
 */
package fr.blitzkrieg.traitements.ia;

import java.util.List ;
import java.util.Random ;

import fr.blitzkrieg.donnees.Case ;
import fr.blitzkrieg.donnees.Joueur ;
import fr.blitzkrieg.donnees.Partie ;
import fr.blitzkrieg.donnees.Terrain ;
import fr.blitzkrieg.donnees.Case.TypeCase ;
import fr.blitzkrieg.traitements.Regles ;



// IA simple, qui ne se comporte pas de manière totalement stupide.
public class IASimple implements IA
{

    private Joueur joueur ;         // Joueur associé à cette IA
    private Partie partie ;         // Partie à laquelle joue le joueur
    private Random genAlea ;        // Générateur de nombres aléatoires à utiliser
    
    
    
    // Constructeur
    public IASimple (Joueur joueur, Partie partie)
    {
        this.joueur  = joueur ;
        this.partie  = partie ;
        // TODO Ce serait mieux de ne pas en créer comme ça n'importe comment : si on crée toutes les IA
        //      "en même temps" elle vont avoir le même générateur. Ce n'est pas très grave, elle vont
        //      prendre des décisions sur des situations différentes, mais on ne sait jamais ; et
        //      utiliser un même générateur pour toute l'appli alors qu'il y a plusieurs fils d'exécution
        //      c'est une mauvaise idée. Il faudrait une ùéthode synchronisée dans l'application qui
        //      permette d'obtenir un générateur aléatoire (en les initialisant avec une séquence de
        //      nombres d'un générateur consacré à ça par exemple)
        this.genAlea = new Random() ;
    }
    
    
    
    // Méthode appelée au début du tour du joueur, qui permet à l'IA de réinitialiser certains éléments
    public void debutTour ()
    {
        // (ne rien faire)
    }
    
    
    // Méthode appelée pour jouer le tour de l'IA
    public ActionIA choisirAction ()
    {
        Terrain terrain = this.partie.terrain() ;
        Regles  regles  = this.partie.regles () ;
        
        
        // Si on est au tour d'initialisation, ne pas proposer d'action
        if (this.partie.tourInitialisation())
            return null ;
        
        // S'il n'y a plus d'actions possibles, ne pas proposer d'action
        // TODO Bof : le nombre de troupes total ne compte pas les troupes sur des cases mortes : ces
        //      vérifications ne servent pas à grand chose, il faut les refaire plus loin
        //        ! Le test sur nbActions == 0 est quand même important
        if (this.joueur.nbActions() == 0 ||
            this.joueur.nbTroupes() == 0 ||
            this.joueur.nbActions() == 1 && this.joueur.nbTroupesReserve() == 0)
        {
            return null ;
        }
        
        // S'il n'y a pas de troupe disponible, retirer une troupe au hasard
        if (this.joueur.nbTroupesReserve() == 0)
        {
            List<Case> casesAvecTroupes = terrain.casesAvecTroupeDisponible (this.joueur) ;
            if (casesAvecTroupes.isEmpty())
                return null ;
            else
                return new ActionIA (ActionIA.RETIRER,
                                     casesAvecTroupes.get (this.genAlea.nextInt (casesAvecTroupes.size()))) ;
        }

        
        // Chercher une case à explorer
        List<Case> casesExploration = UtilesIA.casesExplorables (this.partie, this.joueur) ;
        if (! casesExploration.isEmpty())
            return new ActionIA (ActionIA.ATTAQUER,
                                 casesExploration.get (this.genAlea.nextInt (casesExploration.size()))) ;
        
        // S'il n'y a pas de case à explorer, chercher une case à attaquer
        // (assaut déjà en cours)
        Case caseAssautEnCours = regles.caseAssautEnCours() ;
        if (caseAssautEnCours != null)
            return new ActionIA (ActionIA.ATTAQUER, caseAssautEnCours) ;
        // (ville attaquable par terre)
        List<Case> villesAttaquablesParTerre = UtilesIA.casesAttaquables (this.partie, this.joueur, TypeCase.CASE_VILLE, true) ;
        if (! villesAttaquablesParTerre.isEmpty())
            return new ActionIA (ActionIA.ATTAQUER,
                                 villesAttaquablesParTerre.get (this.genAlea.nextInt (villesAttaquablesParTerre.size()))) ;
        // (case attaquable par terre)
        List<Case> casesAttaquablesParTerre = UtilesIA.casesAttaquables (this.partie, this.joueur, null, true) ;
        if (! casesAttaquablesParTerre.isEmpty())
            return new ActionIA (ActionIA.ATTAQUER,
                                 casesAttaquablesParTerre.get (this.genAlea.nextInt (casesAttaquablesParTerre.size()))) ;
        // (case attaquable)
        List<Case> casesAttaquables = UtilesIA.casesAttaquables (this.partie, this.joueur, null, false) ;
        if (! casesAttaquables.isEmpty())
            return new ActionIA (ActionIA.ATTAQUER,
                                 casesAttaquables.get (this.genAlea.nextInt (casesAttaquables.size()))) ;
        
        // Aucune action intéressante
        return null ;
    }
    
}
