/**
 * 
 * Copyright 2009-2010 Sylvain Lavalley
 * 
 * This file is part of Blitzkrieg.
 *
 * Blitzkrieg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Blitzkrieg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blitzkrieg. If not, see <http://www.gnu.org/licenses/>.
 *  
 */
package fr.blitzkrieg.traitements.ia;

import static fr.blitzkrieg.donnees.Case.TypeCase.CASE_TERRE ;
import static fr.blitzkrieg.donnees.Case.TypeCase.CASE_VILLE ;
import static fr.blitzkrieg.donnees.Terrain.appartenantA ;
import static fr.blitzkrieg.traitements.ia.ActionIA.ATTAQUER ;
import static fr.blitzkrieg.traitements.ia.ActionIA.DEPOSER ;
import static fr.blitzkrieg.traitements.ia.ActionIA.RETIRER ;

import java.util.ArrayList ;
import java.util.HashMap ;
import java.util.List ;
import java.util.Map ;
import java.util.Random ;
import java.util.Map.Entry ;

import fr.blitzkrieg.donnees.Case ;
import fr.blitzkrieg.donnees.Joueur ;
import fr.blitzkrieg.donnees.Partie ;
import fr.blitzkrieg.donnees.Terrain ;



// IA simple analysant simplement la situation.
public class IASimpleAnalyse implements IA
{

    private Joueur joueur ;         // Joueur associé à cette IA
    private Partie partie ;         // Partie à laquelle joue le joueur
    private Random genAlea ;        // Générateur de nombres aléatoires à utiliser
    
    
    
    // Constructeur
    public IASimpleAnalyse (Joueur joueur, Partie partie)
    {
        this.joueur  = joueur ;
        this.partie  = partie ;
        // TODO Ce serait mieux de ne pas en créer comme ça n'importe comment : si on crée toutes les IA
        //      "en même temps" elle vont avoir le même générateur. Ce n'est pas très grave, elle vont
        //      prendre des décisions sur des situations différentes, mais on ne sait jamais ; et
        //      utiliser un même générateur pour toute l'appli alors qu'il y a plusieurs fils d'exécution
        //      c'est une mauvaise idée. Il faudrait une méthode synchronisée dans l'application qui
        //      permette d'obtenir un générateur aléatoire (en les initialisant avec une séquence de
        //      nombres d'un générateur consacré à ça par exemple)
        this.genAlea = new Random() ;
    }
    
    
    
    // Méthode appelée au début du tour du joueur, qui permet à l'IA de réinitialiser certains éléments
    public void debutTour ()
    {
        // (ne rien faire)
    }
    
    
    // Méthode appelée pour jouer le tour de l'IA
    public ActionIA choisirAction ()
    {
        Terrain terrain = this.partie.terrain() ;
        
        
        // S'il n'y a plus d'actions possibles, ne pas proposer d'action
        // TODO Bof : le nombre de troupes total ne compte pas les troupes sur des cases mortes : ces
        //      vérifications ne servent pas à grand chose, il faut les refaire plus loin
        //        ! Le test sur nbActions == 0 est quand même important
        //        ! La première et la deuxième sont importantes, la consultation du nombre total de
        //          troupes... sans doute
        if ((this.joueur.nbActions() == 0 && ! this.partie.tourInitialisation()) ||
            this.joueur.nbTroupes() == 0 ||
            this.joueur.nbActions() == 1 && this.joueur.nbTroupesReserve() == 0)
        {
            return null ;
        }

        
        // Evaluer les options de défense et d'attaque/exploration
        // TODO Ranger les déclarations ailleurs ?
        Map<Case,Integer> valsCasesDefendables = calculerValeursCasesDefendables() ;
        Map<Case,Integer> valsCasesAttaquables = (this.partie.tourInitialisation() ?
                                                    new HashMap() :
                                                    calculerValeursCasesAttaquables()) ;    // TODO S'occuper éventuellement de plans d'attaque plus tard dès le tour de déploiement (quand on saura faire des plans d'attaque)
        
        // Récupérer les options de plus grande valeur
        Case meilleureCaseDefense = meilleureCase (valsCasesDefendables) ;
        Case meilleureCaseAttaque = meilleureCase (valsCasesAttaquables) ;
        int valMeilleureDefense   = (meilleureCaseDefense == null ? 0 : valsCasesDefendables.get (meilleureCaseDefense)) ;
        int valMeilleureAttaque   = (meilleureCaseAttaque == null ? 0 : valsCasesAttaquables.get (meilleureCaseAttaque)) ;

        // S'il n'y a aucune cible intéressante, ne rien faire
        if (valMeilleureDefense == 0 && valMeilleureAttaque == 0)
            return null ;
        
        // Trouver une troupe disponible
        // TODO S'il y a besoin d'enlever une troupe du terrain, évaluer la défense fournie par les troupes sur le terrain
        //      et utiliser la troupe ayant la plus faible utilité (seulement si l'utilité de cette troupe est
        //      inférieure à l'utilité du nouveau déploiement/assaut (nettement inférieur pour les cas de défense,
        //      sinon autant économiser les actions pour plus tard))
        //          => En gros il faut évaluer la valeur de défense si cette troupe n'était pas là :
        //             est-ce qu'on veut la remettre à son ancienne place ou est-ce qu'on veut la mettre
        //             au nouvel endroit
        //              ! Tant que la défense ne concerne que les villes, c'est facile : il suffit de
        //                calculer l'intérêt de mettre cette troupe là en supposant qu'elle n'y est pas
        //                (il suffit de refaire le calcul de défense sur les cases autour) ; mais ça va
        //                se compliquer quand il faudra prendre plus de paramètres en compte, faire plus
        //                de calcul, ... et ne pas oublier des endroits
        //              * Faire une classe PartieFictive, qui hérite de Partie, qui contient une partie
        //                et des exceptions (par exemple modifier les propriétés d'une case [=> remplacer
        //                cette case par une autre]). Et on redéfinit certaines méthodes de base pour qu'elles
        //                piochent dans les exceptions avant d'aller demander les vraies données à la
        //                partie
        //                  => C'est bien pour la séparation de la vraie partie et de la variante et pour
        //                     la clarté du code, mais il faut recréer un lien Regles <-> PartieFictive,
        //                     alors qu'en intégrant tout dans Partie il n'y a strictement rien à toucher
        //                     et on est sûr de n'avoir rien oublié. Et on ne va pas sortir AspectJ pour
        //                     ça... En plus il faut modifier des données dans Terrain, dans Partie, ...
        //                      => Ouais, c'est plus facile de tout intégrer dans les classes concernées
        //                         Le seul inconvénient c'est qu'il ne faut pas oublier de désactiver les
        //                         modifications après utilisation (pas possible de faire une macro ; et
        //                         du coup pas très fiable sans try/finally :( ) ; et pas facile d'empiler
        //                         des variantes et de les dépiler un par une. A voir si utile, pour
        //                         l'instant ce n'est pas nécessaire pour les cas simples ; sinon il faudra
        //                         peut-être tout simplement chaîner les objets (un objet qui en masque un
        //                         autre, qui en masque un autre, ..., chaque objet ne mémorisant que son
        //                         masque, qui mémorise son masque, ...)
        //                  ! Doc : à noter/documenter
        //                      ! Note : les données qui en masquent d'autres sont des copies d'objets,
        //                        donc attention en cas de comparaison par identité
        //                      ! Finalement j'ai fait les modifs au plus près : les modifs sur une case
        //                        sont représentées dans la classe Case, ce n'est pas le terrain qui conserve
        //                        les informations modifiées mais la case elle-même (par exemple sous forme
        //                        d'un objet Case interne qui contient les informations masquantes. De cette
        //                        manière il n'y a plus de problème d'identité, la case interne ne sert qu'à
        //                        stocker les données masquantes, elle n'est jamais présentée au reste du
        //                        programme.
        //              * Eventuellement une opération de Terrain pour s'assurer que toutes les cases sont
        //                revenues à leur état "réel". A voir, en fonction des besoins, pour l'instant il
        //                s'agit de tests léger, ça ne devrait pas poser problème.
        if (this.joueur.nbTroupesReserve() == 0)
        {
            // Lister les troupes disponibles sur le terrain
            List<Case> casesAvecTroupes = terrain.casesAvecTroupeDisponible (this.joueur) ;
            if (casesAvecTroupes.isEmpty())
                return null ;
            
            // Evaluer l'intérêt de ces troupes
            Map<Case,Integer> valsTroupesRetirables = new HashMap() ;
            for (Case c : casesAvecTroupes)
            {
                try
                {
                    Case caseSansTroupe = (Case) c.clone() ;
                    caseSansTroupe.retirerTroupe() ;
                    c.modifCaseMasquante (caseSansTroupe) ;
                    valsTroupesRetirables.put (c, valDefCase (c)) ;
                }
                finally
                {
                    c.modifCaseMasquante (null) ;
                }
            }
            
            // Renvoyer la troupe ayant le plus faible intérêt
            // TODO Seulement si son intérêt est (nettement ?) inférieur à l'intérêt de son nouveau déploiement
            Case caseTroupeALaMoinsUtile = plusMauvaiseCase (valsTroupesRetirables) ;
            return (caseTroupeALaMoinsUtile == null ?
                      null :
                      new ActionIA (RETIRER, caseTroupeALaMoinsUtile)) ;
        }
        
        // Exécuter l'action la plus intéressante
        return  (valMeilleureDefense > valMeilleureAttaque ?
                   new ActionIA (DEPOSER,  meilleureCaseDefense) :
                   new ActionIA (ATTAQUER, meilleureCaseAttaque)) ;
    }
    
    
    // Calcule les valeurs de défense
    private Map<Case,Integer> calculerValeursCasesDefendables ()
    {
        Terrain terrain                        = this.partie.terrain() ;
        List<Case>        casesDefendables     = terrain.casesSansTroupeDisponible (this.joueur) ;
        Map<Case,Integer> valsCasesDefendables = new HashMap() ;
        
        
        // Calculer les scores de défense.
        for (Case c : casesDefendables)
            valsCasesDefendables.put (c, valDefCase (c)) ;
        
        // Renvoyer les scores de défense
        return valsCasesDefendables ;
    }
    
    
    // Calculer la valeur de défense d'une case
    private int valDefCase (Case c)
    {
// TODO Valeurs à ajuster
int    COEFF_DEF_CASE_VILLE = 20 ;     // Coefficient de défense d'une ville
double COEFF_DIST_FRONT     = 2.0 ;    // Coefficient du paramètre de distance au front
double SEUIL_NIVEAU_DEFENSE = 50.0 ;


        Terrain terrain = this.partie.terrain() ;
        int valDefCase   = 0 ;
        
        
        // Défendre les villes
        // TODO Autres (plus tard) : la troupe défend une zone capturée ou empêche sa capture, ...
        int valDefvilles = 0 ;
        List<Case> villesVoisines = appartenantA (this.joueur, terrain.casesVoisines (c, CASE_VILLE)) ;
        for (Case villeVoisine : villesVoisines)
        {
            // TODO A voir : cette méthode ne permet pas de placer les troupes du côté le plus judicieux
            //      (côté front) : et si on considérait la distance entre la case candidate et le front
            //      plutôt qu'entre la ville et le front
            //  int    distFront     = UtilesIA.distanceFront (this.partie, villeVoisine) ;
            int    distFront     = UtilesIA.distanceFront (this.partie, c) ;
            double defenseVille  = this.partie.regles().forceDefense (villeVoisine.x(), villeVoisine.y()) ;
            double niveauDefense = defenseVille + COEFF_DIST_FRONT * distFront ;
            if (niveauDefense < SEUIL_NIVEAU_DEFENSE)
                valDefvilles += (SEUIL_NIVEAU_DEFENSE - niveauDefense) * COEFF_DEF_CASE_VILLE ;
        }
        valDefCase += valDefvilles ;
        
        // Renvoyer le score de la case
        return valDefCase ;
    }
    
    
    // Calcule les scores d'attaque
    private Map<Case,Integer> calculerValeursCasesAttaquables ()
    {
// TODO Valeurs à ajuster (et à ranger)
int COEFF_EXPLORATION     =   2 ;   // Coefficient d'exploration : valeur d'exploration d'une case = (coefficient d'exploration * nombre de cases découvertes en attaquant cette case)
int VAL_CASE_VIDE         =   2 ;   // Valeur de capture d'une case de terre
int VAL_CASE_VILLE        = 100 ;   // Valeur de capture d'une case de ville
int BONUS_ASSAUT_EN_COURS =   1 ;   // Bonus attribué à une case si un assaut est en cours dessus : dans les mêmes conditions, elle va se mettre à dépasser des cases qui avaient le même score [attention à l'exploration, qui diminue quand même quand une case a été attaquée]

        // NOTE Si on ne définit pas spécifiquement dans quel cas on doit poursuivre l'attaque, ça doit
        //      être pris en compte implicitement dans les calculs (on reprend la même décision pour
        //      l'action suivante), voire explicitement (parmis les meilleures cases, choisir celle sur
        //      laquelle un assaut est en cours si elle est dans le tas ; ou encore augmenter la valeur
        //      de la case sur laquelle l'assaut est en cours [ce qui est fait automatiquement si on
        //      tient compte de la probabilité de réussite de l'attaque (sauf si elle est trop faible !])
        Terrain terrain                        = this.partie.terrain() ;
        List<Case>        casesAttaquables     = UtilesIA.casesAttaquables (this.partie, this.joueur, null, false) ;
        Map<Case,Integer> valsCasesAttaquables = new HashMap() ;
        
        // Initialiser les scores
        for (Case c : casesAttaquables)
            valsCasesAttaquables.put (c, 0) ;
        
        // Score d'exploration
        for (Case c : casesAttaquables)
        {
            int scoreExploration = COEFF_EXPLORATION * terrain.nbCasesEnvironnantesNonDecouvertesPar (c.x(), c.y(), this.joueur) ;
            if (scoreExploration != 0)
                valsCasesAttaquables.put (c, valsCasesAttaquables.get (c) + scoreExploration) ;
        }
        
        // Scores de capture
        for (Case c : casesAttaquables)
        {
            int scoreCapture ;
            if      (CASE_TERRE.equals (c.type())) scoreCapture = VAL_CASE_VIDE ;
            else if (CASE_VILLE.equals (c.type())) scoreCapture = VAL_CASE_VILLE ;
            else throw new IllegalStateException ("Type de case inconnu : " + c.type()) ;
            if (scoreCapture != 0)
                valsCasesAttaquables.put (c, valsCasesAttaquables.get (c) + scoreCapture) ;
        }
        
        // Ne pas s'acharner sur les cases mortes
        for (Case c : casesAttaquables)
        {
            if (c.estMorte())
                valsCasesAttaquables.put (c, 0) ;
        }
        
        // Bonus assaut en cours
        Case caseAssautEnCours = this.partie.regles().caseAssautEnCours() ;
        if (caseAssautEnCours != null)
        {
            valsCasesAttaquables.put (caseAssautEnCours, valsCasesAttaquables.get (caseAssautEnCours) + BONUS_ASSAUT_EN_COURS) ;
        }
        
        
        // Renvoyer les scores d'attaque
        return valsCasesAttaquables ;
    }
    
    
    // Sélectionne une case parmi celles de la table en paramètre.
    // Sélectionne la case de plus haute valeur (une au hasard ayant la plus grande valeur en cas d'égalité).
    // Renvoie null s'il n'y a pas de cases dans la table en paramètre.
    private Case meilleureCase (Map<Case,Integer> valsCases)
    {
        // Chercher la plus haute valeur
        int valMax = 0 ;
        for (Entry<Case,Integer> valeur : valsCases.entrySet())
        {
            if (valeur.getValue() > valMax)
                valMax = valeur.getValue() ;
        }
        
        // Récupérer les cases ayant la meilleure valeur
        List<Case> casesSelectionnees = new ArrayList() ;
        for (Entry<Case,Integer> valeur : valsCases.entrySet())
        {
            if (valeur.getValue().intValue() == valMax)
                casesSelectionnees.add (valeur.getKey()) ;
        }
        
        // Renvoyer une case parmi les candidates
        return (casesSelectionnees.isEmpty() ?
                  null :
                  casesSelectionnees.get (this.genAlea.nextInt (casesSelectionnees.size()))) ;
    }
    
    
    // Sélectionne une case parmi celles de la table en paramètre.
    // Sélectionne la case de plus basse valeur (une au hasard ayant la plus petite valeur en cas d'égalité).
    // Renvoie null s'il n'y a pas de cases dans la table en paramètre.
    private Case plusMauvaiseCase (Map<Case,Integer> valsCases)
    {
        // Chercher la plus basse valeur
        int valMin = 0 ;
        for (Entry<Case,Integer> valeur : valsCases.entrySet())
        {
            if (valeur.getValue() < valMin)
                valMin = valeur.getValue() ;
        }
        
        // Récupérer les cases ayant la plus petite valeur
        List<Case> casesSelectionnees = new ArrayList() ;
        for (Entry<Case,Integer> valeur : valsCases.entrySet())
        {
            if (valeur.getValue().intValue() == valMin)
                casesSelectionnees.add (valeur.getKey()) ;
        }
        
        // Renvoyer une case parmi les candidates
        return (casesSelectionnees.isEmpty() ?
                  null :
                  casesSelectionnees.get (this.genAlea.nextInt (casesSelectionnees.size()))) ;
    }
    
}
