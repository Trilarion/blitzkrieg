/**
 * 
 * Copyright 2009-2010 Sylvain Lavalley
 * 
 * This file is part of Blitzkrieg.
 *
 * Blitzkrieg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Blitzkrieg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blitzkrieg. If not, see <http://www.gnu.org/licenses/>.
 *  
 */
package fr.blitzkrieg.traitements.ia;

import static fr.blitzkrieg.donnees.Case.TypeCase.CASE_TERRE ;
import static fr.blitzkrieg.donnees.Case.TypeCase.CASE_VILLE ;
import static fr.blitzkrieg.donnees.Terrain.appartenantA ;
import static fr.blitzkrieg.donnees.Terrain.casesUtilisables ;
import static fr.blitzkrieg.donnees.Terrain.deType ;
import static fr.blitzkrieg.donnees.Terrain.nAppartenantPasA ;
import static fr.blitzkrieg.traitements.ia.ActionIA.ATTAQUER ;
import static fr.blitzkrieg.traitements.ia.ActionIA.DEPOSER ;
import static fr.blitzkrieg.traitements.ia.ActionIA.RETIRER ;
import static fr.blitzkrieg.traitements.ia.Plan.TypePlan.SECURISER_VILLE ;
import static fr.blitzkrieg.traitements.ia.UtilesIA.meilleureCase ;
import static fr.blitzkrieg.traitements.ia.UtilesIA.plusMauvaiseCase ;

import java.util.ArrayList ;
import java.util.HashMap ;
import java.util.Iterator ;
import java.util.List ;
import java.util.Map ;
import java.util.Map.Entry ;
import java.util.Random ;

import fr.blitzkrieg.donnees.Case ;
import fr.blitzkrieg.donnees.Joueur ;
import fr.blitzkrieg.donnees.Partie ;
import fr.blitzkrieg.donnees.Terrain ;
import fr.blitzkrieg.traitements.ia.Plan.TypePlan ;
import fr.blitzkrieg.traitements.ia.chemins.CalculEffortApproximatif ;
import fr.blitzkrieg.traitements.ia.chemins.CalculsChemins ;



// IA standard du jeu, capable de planifier des actions.
public class IAStandard implements IA
{

    private Joueur joueur ;             // Joueur associé à cette IA
    private Partie partie ;             // Partie à laquelle joue le joueur
    private Random genAlea ;            // Générateur de nombres aléatoires à utiliser
    
    private ActionIA actionFuture ;     // Action décidée précédemment et à accomplir (pour éviter que l'analyse de l'action suivante change d'avis)
    
    
    
    // Constructeur
    public IAStandard (Joueur joueur, Partie partie)
    {
        this.joueur  = joueur ;
        this.partie  = partie ;
        // TODO Ce serait mieux de ne pas en créer comme ça n'importe comment : si on crée toutes les IA
        //      "en même temps" elle vont avoir le même générateur. Ce n'est pas très grave, elle vont
        //      prendre des décisions sur des situations différentes, mais on ne sait jamais ; et
        //      utiliser un même générateur pour toute l'appli alors qu'il y a plusieurs fils d'exécution
        //      c'est une mauvaise idée. Il faudrait une méthode synchronisée dans l'application qui
        //      permette d'obtenir un générateur aléatoire (en les initialisant avec une séquence de
        //      nombres d'un générateur consacré à ça par exemple)
        this.genAlea = new Random() ;
    }
    
    
    
    // Méthode appelée au début du tour du joueur, qui permet à l'IA de réinitialiser certains éléments
    public void debutTour ()
    {
        // Ne pas conserver une éventuelle décision du tour précédent
        this.actionFuture = null ;
    }
    
    
    // Méthode appelée pour jouer le tour de l'IA
    public ActionIA choisirAction ()
    {
        Terrain terrain ;                   // Terrain sur lequel se déroule la partie
        int     effortMaximalAttaque ;      // Effort maximal (nombre d'assaut maximal) que le joueur peut réaliser en attaque
        
        
        // Initialisations
        terrain              = this.partie.terrain() ;
        // (le +1 parce que pour la dernière case même s'il y a 90% de chances de prendre la case on compte
        //  qu'il faut un peu plus d'un tour en moyenne, alors qu'il faut le tenter en réalité)
        // TODO Régler ça un peu mieux ? Le calcul d'effort est correct à grande échelle, mais pour une
        //      action précise (prendre la dernière case) il vaut souvent mieux la tenter.
        effortMaximalAttaque = UtilesIA.effortMaximalAttaque (this.joueur) + 1 ;
        
        
        // S'il n'y a plus d'actions possibles, ne pas proposer d'action
        // TODO Bof : le nombre de troupes total ne compte pas les troupes sur des cases mortes : ces
        //      vérifications ne servent pas à grand chose, il faut les refaire plus loin
        //        ! Le test sur nbActions == 0 est quand même important
        //        ! La première et la deuxième sont importantes, la consultation du nombre total de
        //          troupes... sans doute
        if ((this.joueur.nbActions() == 0 && ! this.partie.tourInitialisation()) ||
            this.joueur.nbTroupes() == 0 ||
            (this.joueur.nbActions() == 1 && this.joueur.nbTroupesReserve() == 0))
        {
            return null ;
        }
        
        
        // Si une action a déjà été décidée, l'exécuter
        if (this.actionFuture != null)
        {
            ActionIA action = this.actionFuture ;
            this.actionFuture = null ;
            return action ;
        }

        
        // Evaluer la défense du territoire
        // TODO Ranger les déclarations ailleurs ?
        Map<Case,Integer> valsCasesDefendables = calculerValeursCasesDefendables() ;

        // Générer les plans d'attaque
        // TODO A intégrer dans les calculs d'efforts/intérêts : certains chemins peuvent être plus intéressants
        //      selon les cases capturées
        List<Plan> plansAttaque = genererPlans (this.partie, this.joueur, effortMaximalAttaque, this.partie.tourInitialisation()) ;
        
        // Estimer l'effort nécessaire à l'exécution des plans
        for (Plan plan : plansAttaque)
        {
            if (plan.typePlan() == TypePlan.PRENDRE_VILLE)
                estimerEffortCapture (plan, this.partie, this.joueur, plan.objectif(), effortMaximalAttaque) ;   // CODE Déclarer des variables joueur, partie, ... en local, pour éviter de mettre this devant à chaque fois
            else if (plan.typePlan() == TypePlan.SECURISER_VILLE)
                estimerEffortCapture (plan, this.partie, this.joueur, plan.objectif(), effortMaximalAttaque) ;
            else
                throw new IllegalStateException ("Type de plan non géré : " + plan.typePlan()) ;
        }
        
        // Déterminer la valeur des plans
        evaluerPlans (plansAttaque, effortMaximalAttaque) ;
        
System.out.println() ;
System.out.println ("Effort maximal : " + effortMaximalAttaque) ;
System.out.println ("[Liste des plans [effort estimé] (valeur)]") ;
for (Plan plan : plansAttaque)
    System.out.println (plan.typePlan() + " " + plan.objectif().posEnChaine() + "\t[" + plan.effortEstime() + "]\t(" + plan.valeur() + ")") ;
System.out.println() ;

        // Sélectionner le meilleur plan
        Plan meilleurPlan = meilleurPlan (plansAttaque, this.partie) ;
        
        // Récupérer les options d'action de plus grande valeur
        Case meilleureCaseDefense = meilleureCase (valsCasesDefendables, this.genAlea) ;
        Case meilleureCaseAttaque = (meilleurPlan == null ? null : meilleurPlan.premiereCase()) ;
        int valMeilleureDefense   = (meilleureCaseDefense == null ? 0 : valsCasesDefendables.get (meilleureCaseDefense)) ;
        int valMeilleureAttaque   = (meilleurPlan         == null ? 0 : meilleurPlan.valeur()) ;
System.out.println ("Meilleure option de défense : " + valMeilleureDefense) ;
System.out.println ("Meilleure option d'attaque  : " + valMeilleureAttaque) ;
// ENCOURS Plans
// * L'IA s'occupe trop de défendre les villes loin du front (notamment parce que ça ne dépend pas des
//   moyens d'actions de l'adversaire. Il faudrait défendre surtout les villes attaquables, et on s'en fout
//   du reste (sous le seuil de défense, mais il faut leur donner une valeur de défense quand même pour
//   retirer les troupes loin du front plutôt que les autres). A la limite, même sans tenir compte des
//   moyens de l'adversaire il ne faut pas trop défendre de villes
//      => Si, c'est mieux de tenir compte des moyens de l'adversaire. Par exemple dans la scénario
//         en Russie le Russe a intérêt à défendre ses villes même en profondeur pour éviter que
//         l'Allemagne le lui prenne, parce que l'Allemagne a plus de moyens.
// * Remplacer le calcul d'effort simple par un plus efficace, qui tienne compte des défenses ennemies
//      => Fait partiellement : on ne fait pas le scénarion complet, mais on tient compte des défenses actuelles
// * Trouver une solution pour mieux préparer les assauts : lire les notes sur le sujet et trouver un
//   moyen de mieux préparer les combats, pour avoir plus de chances de réussir une attaque en plaçant
//   quelques troupes devant (déjà ce serait bien si je savais dans quelle mesure c'est utile ; par exemple
//   quand on attaque une ville c'est utile parce que les troupes vont servir en défense)
// => Avec ça on devrait avoir une meilleure méthode d'attaque des villes. Ensuite ce sera réutilisable
//    pour libérer des zones par exemple. Restera à doser et éventuellement combiner plusieurs plans
//    dans l'évaluation pour avoir une première version un peu potable de l'IA (pas très forte, mais
//    qui sait un peu jouer, peut-être de quoi avancer sur les autres sujets et sortir une première version.

        
        // S'il n'y a aucune cible intéressante, ne rien faire
        if (valMeilleureDefense == 0 && valMeilleureAttaque == 0)
            return null ;
        
        
        // Décider de l'action à mener
        // (indépendamment du fait qu'il faille éventuellement retirer une troupe ailleurs)
        ActionIA actionAEffectuer ;
        int      valActionAEffectuer ;
        if (valMeilleureDefense > valMeilleureAttaque)
        {
            actionAEffectuer = new ActionIA (DEPOSER,  meilleureCaseDefense) ;
            valActionAEffectuer = valMeilleureDefense ;
        }
        else
        {
            actionAEffectuer = new ActionIA (ATTAQUER, meilleureCaseAttaque) ;
            valActionAEffectuer = valMeilleureAttaque ;
        }
        
        
        // S'il y a des troupes en réserve, effectuer l'action
        if (this.joueur.nbTroupesReserve() > 0)
        {
            return actionAEffectuer ;
        }
        // Sinon trouver une troupe disponible et reporter l'action
        else
        {
            // Mémoriser l'action
            this.actionFuture = actionAEffectuer ;
            
            // Lister les troupes disponibles sur le terrain
            List<Case> casesAvecTroupes = terrain.casesAvecTroupeDisponible (this.joueur) ;
            if (casesAvecTroupes.isEmpty())
                return null ;
            
            // Evaluer l'intérêt de ces troupes pour la défense
            Map<Case,Integer> valsDefTroupesRetirables = new HashMap() ;
            for (Case c : casesAvecTroupes)
            {
                // Evaluer l'intérêt de cette troupe en défense en simulant un retrait et en calculant
                //   l'intérêt de remettre la troupe à cet endroit
                try
                {
                    Case caseSansTroupe = (Case) c.clone() ;
                    caseSansTroupe.retirerTroupe() ;
                    c.modifCaseMasquante (caseSansTroupe) ;
                    valsDefTroupesRetirables.put (c, valDefCase (c)) ;
                }
                finally
                {
                    c.modifCaseMasquante (null) ;
                }
            }
            
            // Eliminer des troupes retirables celles dont l'intérêt en défense est supérieur à l'intérêt
            //   de l'action à mener
            //     ! Ne pas tenter d'unifier l'intérêt en attaque et l'intérêt en défense : ce sont deux
            //       choses différentes, ce n'est pas le même classement : on élimine d'abord les
            //       troupes qu'on ne veut pas retirer à cause de leur intérêt en défense, puis on classe
            //       les cases restantes
            //          => En fait on devrait pouvoir unifier tout ça, mais ça devient très délicat à doser ;
            //             ça revient à considérer qu'on peut annuler l'attaque parce que les troupes à
            //             retirer ont un intérêt pour l'attaque (ce qui peut être le cas parfois : on voudra
            //             suspendre une attaque plutôt que la lancer avec n'importe quoi ; mais l'évaluation
            //             est très complexe)
            //              ! TODO Il faudrait sans doute l'unifier, ne serait-ce que pour le choix de la troupe à
            //                retirer pour une attaque (il faut prendre en compte son intérêt en défense et
            //                en attaque, si possible de manière combinée ; mais pour l'instant on va se
            //                contenter d'un classement rapide
            Iterator<Entry<Case,Integer>> itCasesTroupes = valsDefTroupesRetirables.entrySet().iterator() ;
            while (itCasesTroupes.hasNext())
            {
                Entry<Case,Integer> entree = itCasesTroupes.next() ;
                int  valDefense = entree.getValue() ;
                if (valDefense >  valActionAEffectuer ||
                    valDefense == valActionAEffectuer && actionAEffectuer.code().equals (DEPOSER))
                {
                    itCasesTroupes.remove() ;
                }
            }
            
            // Choisir la troupe à retirer
            Case caseTroupeLaMoinsUtile ;
            // (si l'action est une défense)
            if (actionAEffectuer.code().equals (DEPOSER))
            {
                // Choisir la troupe qui a le moins d'intérêt en défense
                caseTroupeLaMoinsUtile = plusMauvaiseCase (valsDefTroupesRetirables, this.genAlea) ;
            }
            // (si l'action est une attaque)
            else if (actionAEffectuer.code().equals (ATTAQUER))
            {
                // Créer une liste restreinte de cases, sans celles qui se trouvent à côté de la case à attaquer
                // TODO Donner une valeur convenable, pas faire à l'arrache comme ça
                //          => A base de la perte d'efficacité de l'assaut, de l'intérêt de ces troupes
                //             en défense (pour l'instant on ne s'occupe que de la défense des villes,
                //             mais certaines troupes ont un intérêt comme défense locale), du placement
                //             de ces troupes dans le plan (par exemple si on a une troupe à côté de la
                //             ville asssaillie, sa place est plus importante qu'un troupe qui se trouve
                //             un peu n'importe où et il vaut mieux en prendre une autre)
                //              ! A combiner avec les valeurs de défense : on cherche une troupe pas très
                //                utile en défense et pas trop en attaque non plus
                //                  ! Attention, si on veut simplifier tout ça et combiner les deux dès le
                //                    début, il faut savoir dans quelle mesure la valeur de défense+attaque
                //                    peut interdire cette action (il y a sans doute des cas où c'est
                //                    possible, même ce n'est pas courant ; par contre c'est difficile à
                //                    réaliser come évaluation, surtout si on a déjà pris la décision
                //                    de lancer une attaque plutôt qu'étudier les actions de défense ;
                //                    est-ce que ce n'est pas encore plus haut qu'il faudrait faire ça ?)
                List<Case> casesVoisinesAttaque = terrain.casesVoisines (actionAEffectuer.caseConcernee()) ;
                Map<Case,Integer> valsDefTroupesRetirablesRestreinte = new HashMap() ;
                for (Entry<Case,Integer> entree : valsDefTroupesRetirables.entrySet())
                {
                    Case c = entree.getKey() ;
                    if (! casesVoisinesAttaque.contains (c))
                        valsDefTroupesRetirablesRestreinte.put (entree.getKey(), entree.getValue()) ;
                }
                    
                // S'il reste des cases dans la liste restreinte, piocher dedans, sinon dans la liste complète
                if (! valsDefTroupesRetirablesRestreinte.isEmpty())
                    caseTroupeLaMoinsUtile = plusMauvaiseCase (valsDefTroupesRetirablesRestreinte, this.genAlea) ;
                else
                    caseTroupeLaMoinsUtile = plusMauvaiseCase (valsDefTroupesRetirables, this.genAlea) ;
            }
            else
            {
                throw new IllegalStateException ("Type d'action non géré : " + actionAEffectuer.code()) ;
            }
            
            
            // Retirer la troupe ayant le plus faible intérêt
            return (caseTroupeLaMoinsUtile == null ?
                      null :
                      new ActionIA (RETIRER, caseTroupeLaMoinsUtile)) ;
        }
        
    }
    
    
    // Génère une liste de plans envisageables étant donné l'effort maximal possible.
    // NOTE Oui, il faut prendre ça en compte dès maintenant, pas forcément pour les villes mais
    //      pour la recherche de zones à libérer/isoler.
    private List<Plan> genererPlans (Partie partie, Joueur joueur, int effortMaximal, boolean tourInitialisation)
    {
        Terrain    terrain ;            // Terrain du jeu
        List<Plan> plans ;              // Plans envisageables
        List<Case> villesAmies ;        // Cases de villes amies
        List<Case> villesEnnemies ;     // Cases de villes ennemies

        
        // Actuellement il n'y a pas de plans pour le tour d'initialisation
        if (tourInitialisation)
            return new ArrayList (0) ;

        
        // Initialisations
        terrain = partie.terrain() ;
        plans   = new ArrayList() ;
        villesAmies    = appartenantA     (joueur, deType (CASE_VILLE, terrain.toutesLesCases())) ;
        villesEnnemies = nAppartenantPasA (joueur, deType (CASE_VILLE, terrain.toutesLesCases())) ;
        
        
        // Sécurisation des villes amies
        for (Case caseVille : villesAmies)
        {
            for (Case caseASecuriser : casesUtilisables (nAppartenantPasA (joueur, terrain.casesVoisines (caseVille))))
                plans.add (new Plan (SECURISER_VILLE, caseASecuriser)) ;
        }
        // TODO Faire plus souple. Pour l'instant s'il y a des villes à sécuriser on s'occupe de ça et
        //      de rien d'autre
        //          ! Par exemple si deux villes se touchent ça va être bourrin
        if (! plans.isEmpty())
            return plans ;
        
        
        // Captures de villes ennemies
        for (Case caseVille : villesEnnemies)
        {
            // Conserver les villes à une distance raisonnable (à vol d'oiseau)
            // TODO Est-ce bien utile de faire des estimations à l'arrache (de les noter dans le plan je
            //      veux dire)
            // TODO Supprimer les calculs de distance à vol d'oiseau, à cause des ports. C'est plus simple
            //      de faire un calcul de plus courts chemins (parallélisé, en remplissant une grille et
            //      pas avec une file à priorité. Si on ne tient pas compte de la difficulté à prendre les
            //      cases et donc pas de l'évolution de la situation, ce n'est pas si long. On peut sans doute
            //      limiter la zone à parcourir dans tous les sens en notant les x et y min et max de la
            //      zone considérée, qui grandissent quand on ajoute de nouvelles cases (mouais, euh si
            //      on a un port qui mène à toute la carte, on doit tout parcourir ; utiliser plutôt une
            //      liste de cases (table de hachage ?) évaluées, à faire grandir en partnt des cases connues
            //      et en ajoutant leurs voisines tant que le chemin n'est pas trop long).
            int distanceAuFront = UtilesIA.distanceFront (partie, caseVille) ;
            if (distanceAuFront <= effortMaximal)
                plans.add (new Plan (TypePlan.PRENDRE_VILLE, caseVille)) ;
        }
        
        
        // Renvoyer les plans
        return plans ;
    }
    
    
    // Evalue l'effort estimé pour prendre la case donnée (en général une ville).
    // En paramètre : l'effort maximal consenti.
    // La fonction met à jour la l'effort estimé et la première case du plan.
    // Si la fonction ne trouve pas de chemin pour capturer l'objectif avec un effort inférieur ou
    //   égal à l'effort maximal consenti, elle ne modifie pas le plan.
    private void estimerEffortCapture (Plan plan, Partie partie, Joueur joueur, Case objectif, int effortMaximal)
    {
        Object[] res = CalculsChemins.chercherCourtChemin (partie, joueur, new CalculEffortApproximatif(),
                                                           UtilesIA.casesFront (partie, joueur), objectif,
                                                           effortMaximal) ;
        List<Case> chemin       = (List)   res[0] ;
        Double     effortEstime = (Double) res[1] ;
        if (chemin != null && ! chemin.isEmpty())
            plan.modifEffortEstime (chemin.get(0), effortEstime) ;
        
        // Calculer les distances au front des cases ennemies, avec l'effort maximal fourni en paramètre
        // Regarder si l'objectif est atteignable et renvoyer l'effort estimé.
        // TODO Il faudrait calculer tous les chemins, en tenant compte de l'évolution de la situation (on
        //      a déposé et retiré des troupes), mais la quantité de solutions à envisager augmente
        //      exponentiellement... Pour les longues distances à parcourir sans rencontrer de résistance,
        //      ça ne change pas grand chose. Pour attaquer des cases défendues c'est un peu plus compliqué.
        //      Le mieux est de faire une projection précise sur les quelques prochains tours, ce qui permet
        //      de mettre en place des plans d'assaut avec déploiement de troupes, et de faire des estimations
        //      au-delà (on peut faire ça en nombre de solutions envisagées plutôt qu'en nombre de tours à
        //      l'avance pour éviter que ça ne devienne trop long dans des cas extrêmes).
        //      Pour l'instant on se contente d'estimations, en considérant qu'il y a toujours une troupe
        //      à proximité quand on lance l'assaut (supposition qui peut ne pas être vérifée si on n'a
        //      plus grand chose, mais ce sera un peu gommé quand on fera une étude complète des possibilités
        //      pour les prochains tours).
        //          ! Euh... calculer les derniers tours exhaustivement c'est bien, mais les premiers c'est
        //            moins facile : ça revient à calculer un gros tas de possibilités et à faire le
        //            calcul rapide à partir de chacune d'entre elles.
        //              ! Ca peut donc servir à la fin, pas forcément au début, ce qui peut éventuellement
        //                poser problème s'il y a un obstacle à surmonter. Enfin on peut toujours dire que
        //                s'il y a des ennemis on passe en mode calcul complet et ensuite on reprend le
        //                calcul rapide à partir d'une position unique (la "meilleure" ?)
    }
    
    
    // Calcule la valeur des plans en paramètre
    private void evaluerPlans (List<Plan> plans, double effortMaximal)
    {
        // TODO A améliorer et utiliser des constantes
        // TODO Si l'effort estimé est Double.POSITiITIVE_INFINITY, il faudrait donner une valeur spéciale
        //      (par exemple laisser à null)
        //          => Non, en fait au moment de l'estimation de l'effort il faudrait le mettre à null s'il
        //             a cette valeur spéciale ou s'il dépasse un certain seuil (très très loin de l'effort
        //             maximal)
        for (Plan plan : plans)
        {
            if (plan.effortEstime() != null)
            {
                // Sécurisation des villes
                // TODO (pour l'instant si on a un plan de ce type on n'en a pas d'autres, donc c'est une
                //       comparaison entre plans de même nature ; ne pas modifier en même temps que le reste)
                //          ! On peut avoir des défenses, il faut donner la priorité à a sécurisation (en fait
                //            il fautdrait faire quelque chose de plus subtil : certaines villes complètement
                //            à découvert pourraient avoir besoin de défenses plutôt que de gaspiller beaucoup
                //            d'efforts à sécuriser une ville à côté de laquelle se trouvent beaucoup
                //            d'ennemis
                if (plan.typePlan().equals (SECURISER_VILLE))
                    plan.modifValeurPlan ((int) (10000 - plan.effortEstime())) ;
                // Autres plans
                else
                    plan.modifValeurPlan ((int) (10* (10 + effortMaximal * 2 - plan.effortEstime()))) ;
            }
        }
    }
    
    
    
    // Calcule les valeurs de défense
    private Map<Case,Integer> calculerValeursCasesDefendables ()
    {
        Terrain terrain                        = this.partie.terrain() ;
        List<Case>        casesDefendables     = terrain.casesSansTroupeDisponible (this.joueur) ;
        Map<Case,Integer> valsCasesDefendables = new HashMap() ;
        
        
        // Calculer les scores de défense.
        for (Case c : casesDefendables)
            valsCasesDefendables.put (c, valDefCase (c)) ;
        
        // Renvoyer les scores de défense
        return valsCasesDefendables ;
    }
    
    
    // Calculer la valeur de défense d'une case
    private int valDefCase (Case c)
    {
// TODO Valeurs à ajuster
int    COEFF_DEF_CASE_VILLE = 20 ;     // Coefficient de défense d'une ville
double COEFF_DIST_FRONT     = 2.0 ;    // Coefficient du paramètre de distance au front
double SEUIL_NIVEAU_DEFENSE = 50.0 ;


        Terrain terrain = this.partie.terrain() ;
        int valDefCase   = 0 ;
        
        
        // Défendre les villes
        // TODO Autres (plus tard) : la troupe défend une zone capturée ou empêche sa capture, ...
        // TODO Tenir compte de l'effort amximal déployable par les ennemis ? (avec une limite : si l'ennemi
        //      accumule beaucoup d'actions, il ne faut pas forcément défendre toutesles villes le plus
        //      possible)
        int valDefvilles = 0 ;
        List<Case> villesVoisines = appartenantA (this.joueur, terrain.casesVoisines (c, CASE_VILLE)) ;
        for (Case villeVoisine : villesVoisines)
        {
            //  int    distFront     = UtilesIA.distanceFront (this.partie, villeVoisine) ;
            int    distFront     = UtilesIA.distanceFront (this.partie, c) ;
            double defenseVille  = this.partie.regles().forceDefense (villeVoisine.x(), villeVoisine.y()) ;
            double niveauDefense = defenseVille + COEFF_DIST_FRONT * distFront ;
            if (niveauDefense < SEUIL_NIVEAU_DEFENSE)
                valDefvilles += (SEUIL_NIVEAU_DEFENSE - niveauDefense) * COEFF_DEF_CASE_VILLE ;
        }
        valDefCase += valDefvilles ;
        
        // Renvoyer le score de la case
        return valDefCase ;
    }
    
    
    // Calcule les scores d'attaque
    private Map<Case,Integer> calculerValeursCasesAttaquables ()
    {
// TODO Valeurs à ajuster (et à ranger)
int COEFF_EXPLORATION     =   2 ;   // Coefficient d'exploration : valeur d'exploration d'une case = (coefficient d'exploration * nombre de cases découvertes en attaquant cette case)
int VAL_CASE_VIDE         =   2 ;   // Valeur de capture d'une case de terre
int VAL_CASE_VILLE        = 100 ;   // Valeur de capture d'une case de ville
int BONUS_ASSAUT_EN_COURS =   1 ;   // Bonus attribué à une case si un assaut est en cours dessus : dans les mêmes conditions, elle va se mettre à dépasser des cases qui avaient le même score [TODO attention à l'exploration, qui diminue quand même quand une case a été attaquée]

        // NOTE Si on ne définit pas spécifiquement dans quel cas on doit poursuivre l'attaque, ça doit
        //      être pris en compte implicitement dans les calculs (on reprend la même décision pour
        //      l'action suivante), voire explicitement (parmis les meilleures cases, choisir celle sur
        //      laquelle un assaut est en cours si elle est dans le tas ; ou encore augmenter la valeur
        //      de la case sur laquelle l'assaut est en cours [ce qui est fait automatiquement si on
        //      tient compte de la probabilité de réussite de l'attaque (sauf si elle est trop faible !])
        Terrain terrain                        = this.partie.terrain() ;
        List<Case>        casesAttaquables     = UtilesIA.casesAttaquables (this.partie, this.joueur, null, false) ;
        Map<Case,Integer> valsCasesAttaquables = new HashMap() ;
        
        // Initialiser les scores
        for (Case c : casesAttaquables)
            valsCasesAttaquables.put (c, 0) ;
        
        // Score d'exploration
        for (Case c : casesAttaquables)
        {
            int scoreExploration = COEFF_EXPLORATION * terrain.nbCasesEnvironnantesNonDecouvertesPar (c.x(), c.y(), this.joueur) ;
            if (scoreExploration != 0)
                valsCasesAttaquables.put (c, valsCasesAttaquables.get (c) + scoreExploration) ;
        }
        
        // Scores de capture
        for (Case c : casesAttaquables)
        {
            int scoreCapture ;
            if      (CASE_TERRE.equals (c.type())) scoreCapture = VAL_CASE_VIDE ;
            else if (CASE_VILLE.equals (c.type())) scoreCapture = VAL_CASE_VILLE ;
            else throw new IllegalStateException ("Type de case inconnu : " + c.type()) ;
            if (scoreCapture != 0)
                valsCasesAttaquables.put (c, valsCasesAttaquables.get (c) + scoreCapture) ;
        }
        
        // Ne pas s'acharner sur les cases mortes
        for (Case c : casesAttaquables)
        {
            if (c.estMorte())
                valsCasesAttaquables.put (c, 0) ;
        }
        
        // Bonus assaut en cours
        Case caseAssautEnCours = this.partie.regles().caseAssautEnCours() ;
        if (caseAssautEnCours != null)
        {
            valsCasesAttaquables.put (caseAssautEnCours, valsCasesAttaquables.get (caseAssautEnCours) + BONUS_ASSAUT_EN_COURS) ;
        }
        
        
        // Renvoyer les scores d'attaque
        return valsCasesAttaquables ;
    }

    
    
    // Renvoie le plan le ayant la plaus grande valeur.
    // null si la liste ne contient aucun plan ayant une valeur estimée.
    private Plan meilleurPlan (List<Plan> plans, Partie partie)
    {
        Plan meilleurPlan ;             // Meilleur plan trouvé
        Case caseAssautEnCours ;        // Case sur laquelle un assaut est en cours
        

        // Chercher le meilleur plan
        meilleurPlan      = null ;
        caseAssautEnCours = partie.regles().caseAssautEnCours() ;
        for (Plan plan : plans)
        {
            if (plan.valeur() != null)
            {
                // (privilégier la case qu'on est en train d'assaillir... un peu de constance ! d'autant que
                //  les évaluations ne savent pas toujours prendre en compte la bonus d'assaut en cours)
                //     => TODO ce n'est qu'un bricolage : si un assaut échoué a fait baisser la valeur du
                //             plan il se peut qu'il soit passé  derrière d'autres solutions alors qu'on n'a
                //             pas pris en compte le bonus d'assaut en cours
                //              ! On règlera ça partiellement avec une planification plus poussée (meilleures
                //                estimations de l'effort) et en partie en privilégiant le plan en cours
                //                (pour éviter qu'un assaut échouer fasse immédiatement changer l'IA d'avis
                //                 au profit d'une autre action ; et qu'elle revienne au plan une action
                //                 plus tard : favoriser la constance dans l'application des plans, une fois
                //                 qu'on saura doser plus précisément les valeurs des actions)
                if (meilleurPlan          == null         ||
                    meilleurPlan.valeur() == null         ||
                    plan.valeur() > meilleurPlan.valeur() ||
                    (plan.valeur() == meilleurPlan.valeur() && plan.premiereCase().equals (caseAssautEnCours)))
                {
                    meilleurPlan = plan ;
                }
            }
        }
        
        // Renvoyer le plan
        return meilleurPlan ;
    }
    
}



// Représente un plan d'action
// TODO Peut-être séparer en plusieurs sous-classes pour éviter d'entasser les paramètres de tous les
//      types de plans dans la même.
class Plan
{
    
    // Constanes
    public static enum TypePlan {PRENDRE_VILLE,         // Prendre une ville désignée
                                 SECURISER_VILLE,       // Prendre une case aux abords d'une ville pour sécuriser la ville
                                 LIBERER_ZONE,
                                 ISOLER_ZONE} ;
    
    // Données
    private TypePlan typePlan ;         // Type de plan
    private Case     objectif ;         // Objectif (pour une attaque de ville)
    private Double   effortEstime ;     // Effort estimé pour réaliser ce plan (null si effort non estimé)
    private Case     premiereCase ;     // Première case à jouer pour exécuter ce plan
    private Integer  valeur ;           // Valeur accordée au plan (null si non évalué)
    

    
    // Constructeur
    public Plan (TypePlan typePlan, Case objectif)
    {
        this.typePlan     = typePlan ;
        this.objectif     = objectif ;
    }
    
    
    // Modifie l'effort estimé pour ce plan
    public void modifEffortEstime (Case premiereCase, double effortEstime)
    {
        this.premiereCase = premiereCase ;
        this.effortEstime = effortEstime ;
    }
    
    // Modifie la valeur du plan
    public void modifValeurPlan (int valeur)
    {
        this.valeur = valeur ;
    }


    // Accesseurs
    public TypePlan typePlan     () { return this.typePlan ; }
    public Case     objectif     () { return this.objectif ; }
    public Double   effortEstime () { return this.effortEstime ; }
    public Case     premiereCase () { return this.premiereCase ; }
    public Integer  valeur       () { return this.valeur ; }
    
}
