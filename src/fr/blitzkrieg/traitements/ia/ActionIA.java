/**
 * 
 * Copyright 2009-2010 Sylvain Lavalley
 * 
 * This file is part of Blitzkrieg.
 *
 * Blitzkrieg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Blitzkrieg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blitzkrieg. If not, see <http://www.gnu.org/licenses/>.
 *  
 */
package fr.blitzkrieg.traitements.ia;

import fr.blitzkrieg.donnees.Case ;



// Action décidée par l'IA
public class ActionIA
{

    // TODO Voir si c'est juste un conteneur de données ou si on utlise le polymorphisme pour exécuter
    //      facilement les actions (=> les classe renvoyées seraient des sous-classes capables d'exécuter
    //      les actions dans le jeu)
    //          ! Plutôt un conteneur de données : ça permet de regarder l'action et éventuellement de
    //            faire quelque chose avec ça (quelque chose qui ne pourrait pas se coder dans l'action parce
    //            que ça dépendrait du joueur ? Qu'est-ce que le joueur peut bien décider en dehors de l'IA ?
    //            des trucs diplomatiques qui nécessitent des négociations ? Bof, ce n'est pas la partie de
    //            l'IA chargée de choisir une action précise qui va décider d'un truc à négocier...)
    // TODO Une classe ou un code pour "pas d'action" ? Pour traiter plus uniformément les actions (ne
    //      serait-ce que les afficher)
    
    public static String RETIRER  = "retirer" ;
    public static String DEPOSER  = "deposer" ;
    public static String ATTAQUER = "attaquer" ;

    
    private String code ;
    private Case   caseConcernee ;
    
    
    
    // Constructeur
    public ActionIA (String code, Case caseConcernee)
    {
        this.code          = code ;
        this.caseConcernee = caseConcernee ;
    }
    
    
    // Accesseurs
    public String code ()
    {
        return this.code ;
    }
    public Case caseConcernee ()
    {
        return this.caseConcernee ;
    }
    
    
    // Renvoie une représentation de l'action sous forme de chaîne
    public String toString ()
    {
        return this.code + " en " + this.caseConcernee.x() + ", " + this.caseConcernee.y() ;
    }
    
}
