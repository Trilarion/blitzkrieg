/**
 * 
 * Copyright 2009-2010 Sylvain Lavalley
 * 
 * This file is part of Blitzkrieg.
 *
 * Blitzkrieg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Blitzkrieg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blitzkrieg. If not, see <http://www.gnu.org/licenses/>.
 *  
 */
package fr.blitzkrieg.stockage;

import java.awt.Color ;
import java.io.BufferedInputStream ;
import java.io.BufferedOutputStream ;
import java.io.BufferedReader ;
import java.io.FileInputStream ;
import java.io.FileOutputStream ;
import java.io.IOException ;
import java.io.InputStream ;
import java.io.InputStreamReader ;
import java.io.OutputStream ;
import java.io.OutputStreamWriter ;
import java.io.PrintWriter ;
import java.io.Reader ;
import java.util.ArrayList ;
import java.util.List ;
import java.util.Properties ;
import java.util.zip.GZIPInputStream ;
import java.util.zip.GZIPOutputStream ;

import fr.blitzkrieg.donnees.Case ;
import fr.blitzkrieg.donnees.Joueur ;
import fr.blitzkrieg.donnees.Partie ;
import fr.blitzkrieg.donnees.Terrain ;
import fr.blitzkrieg.donnees.Case.TypeCase ;
import fr.blitzkrieg.traitements.GenerateurCarteAleatoire;
import fr.blitzkrieg.traitements.Regles ;



// Sauvegarde et chargement de scénarios et parties
// TODO A voir : pas hyper subtil la sauvegarde du terrain ; on pourrait par exemple
//      mettre des noms moins longs ; on pourrait aussifaire des trucs plus compliqués
//      comme essayer de définir le terrain par des rectangles ayant la même valeur
//      pour une propriété, ... ; on pourrait aussi compresser les sauvegardes
//          => Bof pour le trucs subtil, ça risque d'être plus ou moins efficace
//             selon les cas et compliqué à faire ; laissons faire la compression pour
//             gérer le répétitions, c'est déjà assez efficace (même avec GZip/Zip
//             présents en standard sur le JVM)
//          => Ce serait bien que le chargeur sache lire des sauvegardes compressées
//             ou non (et que la fonction de sauvegarde sache compresser les
//             sauvegardes ou pas),pour avoir des sauvegardes non compressées pour faire
//             des modifs à la main (débogage, création/correction de scénarios à la
//             main, ...)
// TODO Peut-être que des informations comme le nombre de troupes qu'un joueur possède sur le terrain ne
//      devraient pas être stockées mais recalculées ? Ou alors stockées et recalculées, pour vérification
//      (avec un simple avertissement si ce n'est pas correct, il suffit de prendre la valeur calculée) ?
public class StockagePartie
{

    private static String ENCODAGE_SAUVEGARDES   = "UTF-8" ;                            // Encodage des fichiers de sauvegarde
    private static String COMMENTAIRE_DEBUT_SAUVEGARDE = "## Sauvegarde Blitzkrieg" ;   // Commentaire marquant le début des sauvegardes du jeu
    private static String COMMENTAIRE_DEBUT_SCENARIO   = "## Scenario Blitzkrieg" ;     // Commentaire marquant le début des scénarios du jeu NOTE Sans accent pour que n'importe qui puisse les créer facilement à la main [ouais, euh... enfin les cases c'est pas facilé à créer à la main]
    
    
    
    // Sauvegarde une partie dans un fichier
    // TODO Si le fichier existe, il faudrait demander (en dehors d'ici) si on veut
    //      l'écraser ; et dans ce cas il faut faire la savegare dans un fichier à côté
    //      qui remplace ensuite le vrai fichier [ça on le gère ici]
    // NOTE Pas d'espaces avant et surtout après les "=", parce que pour les textes
    //      il faut conserver tous les espaces (donc pas de trim() [pour les textes])
    // TODO Il semblerait que les méthodes de PrintWriter ne lèvent pas d'exception :
    //      est-ce qu'il faut vérifier qu'il n'y a pas eu d'erreur en appelant une méthode ?
    // TODO On pourrait mettre les infos d'une ligne (clé et valeur) dans un tableau
    //      pour un morceau (par exemple pour les joueurs) et ensuite une fonction pour
    //      les écrire dans le fichier ; ça permettrait d'aligner les valeurs les
    //      unes avec les autres en ajoutant des espaces avant d'écrire le "="
    public void sauvegarder (Partie partie, String chemFic, boolean compression) throws IOException
    {
System.out.println ("Sauvegarde") ;
        // Ouvrir le fichier
        FileOutputStream ecrFic = new FileOutputStream     (chemFic) ;
        OutputStream     ecrBin = new BufferedOutputStream (ecrFic) ;
        if (compression)
            ecrBin = new GZIPOutputStream (ecrBin) ;
        PrintWriter ecr = new PrintWriter (new OutputStreamWriter (ecrBin, ENCODAGE_SAUVEGARDES),
                                           true) ;
        
        try
        {
            // Commentaire initial
            ecr.println (COMMENTAIRE_DEBUT_SAUVEGARDE) ;
            ecr.println() ;
            
            // Infos générales sur la partie
            ecr.println ("partie.nom="                      + partie.nom()) ;
            ecr.println ("partie.description="              + partie.description()) ;
            ecr.println ("partie.nbTours="                  + partie.nbTours()) ;
            ecr.println ("partie.nbPointsCase="             + partie.nbPointsCase()) ;
            ecr.println ("partie.nbPointsVille="            + partie.nbPointsVille()) ;
            ecr.println ("partie.invasionsNavalesPermises=" + partie.invasionsNavalesPermises()) ;
            ecr.println ("partie.numTour="                  + partie.numTour()) ;
            ecr.println ("partie.iJoueurCourant="           + partie.iJoueurCourant()) ;
            ecr.println ("partie.commencee="                + partie.commencee()) ;
            ecr.println ("partie.terminee="                 + partie.terminee()) ;
            ecr.println() ;
            
            // Joueurs
            List<Joueur> joueurs = partie.joueurs() ;
            ecr.println ("joueurs.nombre=" + joueurs.size()) ;
            ecr.println() ;
            for (int i = 0 ; i < joueurs.size () ; i++)
            {
                Joueur joueur = joueurs.get (i) ;
                ecr.println ("joueur." + i + ".nom="     + joueur.nom()) ;
                ecr.println ("joueur." + i + ".couleur=" + Integer.toHexString (joueur.couleur().getRGB())) ;
                ecr.println ("joueur." + i + ".humain="  + joueur.estHumain()) ;
                ecr.println ("joueur." + i + ".actif="   + joueur.estActif()) ;
                ecr.println ("joueur." + i + ".armee.efficacite="              + joueur.efficacite()) ;
                ecr.println ("joueur." + i + ".armee.nbTroupesMax="            + joueur.nbTroupesMax()) ;
                ecr.println ("joueur." + i + ".armee.nbTroupesParTour="        + joueur.nbTroupesParTour()) ;
                ecr.println ("joueur." + i + ".armee.nbTroupesParVille="       + joueur.nbTroupesParVille()) ;
                ecr.println ("joueur." + i + ".armee.nbActionsParTour="        + joueur.nbActionsParTour()) ;
                ecr.println ("joueur." + i + ".armee.nbActionsParVille="       + joueur.nbActionsParVille()) ;
                ecr.println ("joueur." + i + ".partie.nbActions="              + joueur.nbActions()) ;
                ecr.println ("joueur." + i + ".partie.nbTroupesReserve="       + joueur.nbTroupesReserve()) ;
                ecr.println ("joueur." + i + ".partie.nbTroupesTerrain="       + joueur.nbTroupesTerrain()) ;
                ecr.println ("joueur." + i + ".partie.nbPointsVictoire="       + joueur.nbPointsVictoire()) ;
                ecr.println ("joueur." + i + ".stats.nbAttaquesLancees="       + joueur.nbAttaquesLancees()) ;
                ecr.println ("joueur." + i + ".stats.nbAttaquesReussies="      + joueur.nbAttaquesReussies()) ;
                ecr.println ("joueur." + i + ".stats.probaReussiteAttMoyenne=" + joueur.probaReussiteAttMoyenne()) ;
                ecr.println ("joueur." + i + ".stats.nbDefenses="              + joueur.nbDefenses()) ;
                ecr.println ("joueur." + i + ".stats.nbDefensesReussies="      + joueur.nbDefensesReussies()) ;
                ecr.println ("joueur." + i + ".stats.probaReussiteDefMoyenne=" + joueur.probaReussiteDefMoyenne()) ;
                ecr.println() ;
    
            }
            
            // Terrain
            Terrain terrain = partie.terrain() ;
            ecr.println ("terrain.largeur=" + terrain.largeur()) ;
            ecr.println ("terrain.hauteur=" + terrain.hauteur()) ;
            for (int x = 0 ; x < terrain.largeur() ; x++)
            for (int y = 0 ; y < terrain.hauteur() ; y++)
            {
                Case caseCourante = terrain.caseEn (x, y) ;
                ecr.println (x + "," + y + ".type="           + caseCourante.type()) ;
                ecr.println (x + "," + y + ".proprietaire="   + joueurs.indexOf (caseCourante.proprietaire())) ;
                ecr.println (x + "," + y + ".troupePresente=" + caseCourante.troupePresente()) ;
                ecr.println (x + "," + y + ".vivante="        + caseCourante.estVivante()) ;
                ecr.print   (x + "," + y + ".decouvertes=") ;
                for (Joueur j : caseCourante.joueursDecouverte())
                    ecr.print (joueurs.indexOf(j) + ", ") ;
                ecr.println() ;
            }
            
            // Fermer le fichier
            ecr.flush() ;           // (vide le tampon des flux suivants aussi)
// TODO Tester ça en arrêtant la sauvegarde avant et après flush() pour vérifier que ça écrit tout
//      bien dans le fichier (arrêt avec System.exit(-1))
            ecrFic.getFD().sync() ;
            ecr.close() ;
            ecr = null ;
        }
        catch (Throwable e)
        {
            // Essayer de fermer le fichier, mais sans masquer l'erreur d'origine
            // (note : pas la peine de s'acharner à écrire le plus de choses possible dans
            //  le fichier, on le ferme juste pour ne pas laisser des descripteurs ouverts
            //  pour rien autant que possible)
            try     { ecr.close() ; }
            finally {}
            
            // Faire suivre l'exception d'origine
            throw new IOException ("Erreur lors de la sauvegarde de la partie", e) ;
        }
    }
    
    
    // Charge une partie depuis un fichier
    public Partie charger (String chemFic) throws IOException
    {
        // Déterminer s'il s'agit d'une sauvegarde compressée
        // TODO Fonction premLigne qui récupère la première ligne d'un fichier
        BufferedReader lectTest = new BufferedReader (new InputStreamReader (new FileInputStream (chemFic),
                                                                             ENCODAGE_SAUVEGARDES)) ;
        String premLigne = null ;
        try     { premLigne = lectTest.readLine() ; }
        finally { lectTest.close() ; }
        boolean compression = premLigne == null ||
                              ! (premLigne.startsWith (COMMENTAIRE_DEBUT_SAUVEGARDE) || premLigne.startsWith (COMMENTAIRE_DEBUT_SCENARIO)) ;
        
        // Charger les données de la sauvegarde
        // TODO try/finally
        Properties props = new Properties() ;
        InputStream lectBin = new BufferedInputStream (new FileInputStream (chemFic)) ;
        if (compression)
            lectBin = new GZIPInputStream (lectBin) ;
        Reader lect = new InputStreamReader (lectBin, ENCODAGE_SAUVEGARDES) ;
        props.load (lect) ;
        lect.close() ;
        
        // Charger les informations sur les joueurs
        int nbJoueurs = lireEntier (props, "joueurs.nombre") ;
        List<Joueur> joueurs = new ArrayList (nbJoueurs) ;
        for (int i = 0 ; i < nbJoueurs ; i++)
        {
            joueurs.add (
                new Joueur (
                    lireChaine  (props, "joueur." + i + ".nom"),
                    lireCouleur (props, "joueur." + i + ".couleur"),
                    lireBooleen (props, "joueur." + i + ".humain"),
                    lireBooleen (props, "joueur." + i + ".actif"),
                    lireDouble  (props, "joueur." + i + ".armee.efficacite"),
                    lireEntier  (props, "joueur." + i + ".armee.nbTroupesMax"),
                    lireEntier  (props, "joueur." + i + ".armee.nbTroupesParTour"),
                    lireEntier  (props, "joueur." + i + ".armee.nbTroupesParVille"),
                    lireEntier  (props, "joueur." + i + ".armee.nbActionsParTour"),
                    lireEntier  (props, "joueur." + i + ".armee.nbActionsParVille"),
                    lireEntier  (props, "joueur." + i + ".partie.nbActions"),
                    lireEntier  (props, "joueur." + i + ".partie.nbTroupesReserve"),
                    lireEntier  (props, "joueur." + i + ".partie.nbTroupesTerrain"),
                    lireEntier  (props, "joueur." + i + ".partie.nbPointsVictoire"),
                    // (les stats sont optionnelles : elles n'apparaissent pas dans les scénarios par exemple)
                    lireEntier  (props, "joueur."  + i + ".stats.nbAttaquesLancees",       0),
                    lireEntier  (props, "joueur."  + i + ".stats.nbAttaquesReussies",      0),
                    lireDouble  (props, "joueur."  + i + ".stats.probaReussiteAttMoyenne", 0.0),
                    lireEntier  (props, "joueur."  + i + ".stats.nbDefenses",              0),
                    lireEntier  (props, "joueur."  + i + ".stats.nbDefensesReussies",      0),
                    lireDouble  (props, "joueur."  + i + ".stats.probaReussiteDefMoyenne", 0.0)
                )) ;
        }
        
        // Charger les informations sur le terrain
        Terrain terrain ;
        int largeur = lireEntier (props, "terrain.largeur") ;
        int hauteur = lireEntier (props, "terrain.hauteur") ;
        boolean terrainAleatoire = lireBooleen (props, "terrain.generationAleatoire", false) ;
        if (terrainAleatoire)
        {
            long graineAleatoire = lireEntierLong (props, "terrain.graineAleatoire", System.currentTimeMillis()) ;
            terrain = new GenerateurCarteAleatoire().genererCarte (joueurs, largeur, hauteur, graineAleatoire) ;
            terrain.mettreAJourVisibiliteJoueurs() ;
        }
        else
        {
            Case[][] cases = new Case[largeur][hauteur] ;
            for (int x = 0 ; x < largeur ; x++)
            for (int y = 0 ; y < hauteur ; y++)
            {
                cases[x][y] = new Case (x,
                                        y,
                                        lireTypeCase         (props, x + "," + y + ".type"),
                                        (Joueur) lireElement (props, x + "," + y + ".proprietaire", joueurs),
                                        lireBooleen          (props, x + "," + y + ".troupePresente"),
                                        lireBooleen          (props, x + "," + y + ".vivante"),
                                        lireListeElements    (props, x + "," + y + ".decouvertes", joueurs)
                                       ) ;
            }
            terrain = new Terrain (largeur, hauteur, cases) ;
        }
        
        // Charge les informations sur la partie
        // NOTE Pour l'instant, jeu de règles unique
        Partie partie = new Partie (new Regles(),
                                    lireChaine  (props, "partie.nom"),
                                    lireChaine  (props, "partie.description"),
                                    lireEntier  (props, "partie.nbTours"),
                                    lireEntier  (props, "partie.nbPointsCase"),
                                    lireEntier  (props, "partie.nbPointsVille"),
                                    lireBooleen (props, "partie.invasionsNavalesPermises"),
                                    joueurs,
                                    terrain,
                                    lireEntier  (props, "partie.numTour"),
                                    lireEntier  (props, "partie.iJoueurCourant"),
                                    lireBooleen (props, "partie.commencee"),
                                    lireBooleen (props, "partie.terminee")
                                   ) ;
        
        // Renvoyer la partie chargée
        partie.regles().associerPartie (partie) ;
        return partie ;
    }
    
    
    
// TODO Arranger tout ça (exception lors des conversions en nombres, ...)
//          ! Mettre les conversions dans une classe de conversions ? Une classe destinée à convertir
//            des données récupérées comme ça d'une source chaîne ?
    // Renvoie la valeur d'une propriété sous la forme d'une chaîne
    // Erreur si la propriété n'existe pas dans le fichier
    // TODO Eclaircir cette question : est-ec qu'il faut ou non lever une erreur si la propriété
    //      n'existe pas ? Parce que là la spécification de la méthode et son implémentation ne sont pas
    //      cohérentes
    private String lireChaine (Properties props, String nomProp)
    {
        return props.getProperty (nomProp) ;
    }
    
    
    // Renvoie la valeur d'une propriété sous la forme d'un entier
    // Erreur si la propriété n'existe pas dans le fichier
    private int lireEntier (Properties props, String nomProp)
    {
        String val = (String) props.get (nomProp) ;
        if (val == null || val.isEmpty())
            throw new IllegalArgumentException ("Pas de propriété ayant ce nom : " + nomProp) ;
        return Integer.parseInt (val.trim()) ;
    }
    
    
    // Renvoie la valeur d'une propriété sous la forme d'un entier
    // La valeur par défaut est renvoyée si la propriété n'existe pas dans le fichier
    private int lireEntier (Properties props, String nomProp, int valDefaut)
    {
        String val = (String) props.get (nomProp) ;
        if (val == null || val.isEmpty())
            return valDefaut ;
        else
            return Integer.parseInt (val.trim()) ;
    }
    
    
    // Renvoie la valeur d'une propriété sous la forme d'un entier en hexédécimal
    // NOTE Note sur le format et son interprétation : là on l'interprète comme une représentation
    //      du binaire, donc le nombre résultat peut être négatif alors que ça n'apparaît pas en
    //      hexadécimal
    //          => Renommer ?
    // Erreur si la propriété n'existe pas dans le fichier
    private int lireEntierHexa (Properties props, String nomProp)
    {
        String val = (String) props.get (nomProp) ;
        if (val == null || val.isEmpty())
            throw new IllegalArgumentException ("Pas de propriété ayant ce nom : " + nomProp) ;
        long valLong = Long.parseLong (val.trim(), 16) ;
        int valInt = 0 ;
        valInt |= valLong ;
        return valInt ;
    }
    
    
//    // Renvoie la valeur d'une propriété sous la forme d'un entier long
//    // Erreur si la propriété n'existe pas dans le fichier
//    private long lireEntierLong (Properties props, String nomProp)
//    {
//        String val = (String) props.get (nomProp) ;
//        if (val == null || val.isEmpty())
//            throw new IllegalArgumentException ("Pas de propriété ayant ce nom : " + nomProp) ;
//        return Long.parseLong (val.trim()) ;
//    }
    
    
    // Renvoie la valeur d'une propriété sous la forme d'un entier long
    // La valeur par défaut est renvoyée si la propriété n'existe pas dans le fichier
    private long lireEntierLong (Properties props, String nomProp, long valDefaut)
    {
        String val = (String) props.get (nomProp) ;
        if (val == null || val.isEmpty())
            return valDefaut ;
        else
            return Long.parseLong (val.trim()) ;
    }

    
    // Renvoie la valeur d'une propriété sous la forme d'un double
    // Erreur si la propriété n'existe pas dans le fichier
    private double lireDouble (Properties props, String nomProp)
    {
        String val = (String) props.get (nomProp) ;
        if (val == null || val.isEmpty())
            throw new IllegalArgumentException ("Pas de propriété ayant ce nom : " + nomProp) ;
        return Double.parseDouble (val.trim()) ;
    }
    
    
    // Renvoie la valeur d'une propriété sous la forme d'un double
    // La valeur par défaut est renvoyée si la propriété n'existe pas dans le fichier
    private double lireDouble (Properties props, String nomProp, double valDefaut)
    {
        String val = (String) props.get (nomProp) ;
        if (val == null || val.isEmpty())
            return valDefaut ;
        else
            return Double.parseDouble (val.trim()) ;
    }
    
    
    // Renvoie la valeur d'une propriété sous la forme d'un booléen
    // Erreur si la propriété n'existe pas dans le fichier
    private boolean lireBooleen (Properties props, String nomProp)
    {
        String val = (String) props.get (nomProp) ;
        if (val == null || val.isEmpty())
            throw new IllegalArgumentException ("Pas de propriété ayant ce nom : " + nomProp) ;
        return Boolean.parseBoolean (val.trim()) ;
    }
    
    
    // Renvoie la valeur d'une propriété sous la forme d'un booléen
    // Renvoie la valeur par défaut si la propriété n'existe pas dans le fichier
    // TODO A voir : pour les valeurs par défaut, en principe on peut utiliser un autre objet Properties,
    //      ce serait peut-être mieux
    private boolean lireBooleen (Properties props, String nomProp, boolean valDefaut)
    {
        String val = (String) props.get (nomProp) ;
        if (val == null || val.isEmpty())
            return valDefaut ;
        else
            return Boolean.parseBoolean (val.trim()) ;
    }
    
    
    // Renvoie la valeur d'une propriété sous la forme d'une couleur
    // Erreur si la propriété n'existe pas dans le fichier
    private Color lireCouleur (Properties props, String nomProp)
    {
        return new Color (lireEntierHexa (props, nomProp), true) ;
    }
    
    
    // Lit la valeur d'une propriété, qui doit être un nombre
    // Pioche dans la liste d'objets en paramètre l'objet au rang indiqué dans la propriété et le renvoie
    // Si le nombre est négatif, renvoie null
    // Erreur si la propriété n'existe pas dans le fichier
    private Object lireElement (Properties props, String nomProp, List elements)
    {
        Integer valInt = lireEntier (props, nomProp) ;
        if (valInt < 0)
            return null ;
        else
            return elements.get (valInt) ;
    }
    
    
    // Renvoie la valeur d'une propriété sous la forme d'une liste d'entiers
    private List lireListeEntiers (Properties props, String nomProp)
    {
        // CODE Code tout moche à arranger (et les ofnctions voisines aussi)
        String val = lireChaine (props, nomProp) ;
        if (val == null)
            throw new IllegalArgumentException ("Pas de propriété ayant ce nom : " + nomProp) ;
        if (val.trim().isEmpty())
            return new ArrayList (0) ;
        String[] valsCh = val.trim().split (",") ;
        List<Integer> valsInt = new ArrayList (valsCh.length) ;
        for (String ch : valsCh)
            valsInt.add (new Integer (ch.trim())) ;
        return valsInt ;
    }

    
    // Lit la valeur d'une propriété, qui doit être une séquence de nombres séparés par des virgules
    // Pioche dans la liste d'objets en paramètre les objets aux rangs indiqués dans la propriété
    //   et renvoie la liste des objets sélectionnés
    // Erreur si la propriété n'existe pas dans le fichier
    private List lireListeElements (Properties props, String nomProp, List elements)
    {
try
{
        List<Integer> valsInt = lireListeEntiers (props, nomProp) ;
        List eltsSelectionnes = new ArrayList (valsInt.size()) ;
        for (Integer val : valsInt)
            eltsSelectionnes.add (elements.get (val)) ;
        return eltsSelectionnes ;
}
catch (RuntimeException e)
{
    System.err.println ("Valeur de la propriété " + nomProp + ": " + props.getProperty (nomProp)) ;
    throw e ;
}
    }
    
    
    // Renvoie la valeur d'une propriété sous la forme d'une valeur de type de case
    // Erreur si la propriété n'existe pas dans le fichier
    // TODO Esr-ce qu'on peut faire un truc plus générique (si les énumérations sont capables de
    //      fournir la liste de leurs valeurs) ?
    private TypeCase lireTypeCase (Properties props, String nomProp)
    {
        String val = (String) props.get (nomProp) ;
        if (val == null || val.isEmpty())
            throw new IllegalArgumentException ("Pas de propriété ayant ce nom : " + nomProp) ;
        val = val.trim() ;
        if      (val.equals (TypeCase.CASE_MER.toString()))   return TypeCase.CASE_MER ;
        else if (val.equals (TypeCase.CASE_TERRE.toString())) return TypeCase.CASE_TERRE ;
        else if (val.equals (TypeCase.CASE_VILLE.toString())) return TypeCase.CASE_VILLE ;
        else throw new IllegalStateException ("Type de case inconnu : " + val) ;
    }
    
}
