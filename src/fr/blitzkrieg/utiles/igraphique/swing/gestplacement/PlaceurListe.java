/**
 * 
 * Copyright 2009-2010 Sylvain Lavalley
 * 
 * This file is part of Blitzkrieg.
 *
 * Blitzkrieg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Blitzkrieg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blitzkrieg. If not, see <http://www.gnu.org/licenses/>.
 *  
 */
package fr.blitzkrieg.utiles.igraphique.swing.gestplacement;

import java.awt.Component ;
import java.awt.Container ;
import java.awt.Dimension ;
import java.awt.LayoutManager ;
import java.awt.LayoutManager2 ;
import java.util.HashMap ;
import java.util.Map ;



// Gestionnaire de placement de composants gérant une liste horizontale ou verticale de composants.
// Ce gestionnaire de placement ressemble à ce que sont censées faire BoxLayout et le conteneur Box,
//   mais en plus simple. Box et BoxLayout sont trop capricieux (par exemple sur une boîte verticale tant
//   que je mettais des JLabel ça allait (ils étaient alignés à gauche), mais quand j'ai ajouté un
//   composant perso contenant une image et ayant une taille préférée, une taille minimale et une taille
//   maximale les composant se sont tous trouvés alignés bizarrement (BoxLayout tente de faire des
//   alignements sur je sais pas quoi apparemment). Et suivant si le composant avait ou non une taille
//   minimale et avait ou non une taille maximale, le résultat était très différent. BoxLayout semble
//   se baser sur cette information pour décider comment placer les compsoants, selon un rituel mystérieux.
// TODO Tests unitaires
public class PlaceurListe implements LayoutManager, LayoutManager2
{
    
/*
 * TODO Compléter ce gestionnaire de placement
 *          * Gérer correctement les bordures : quand on a une bordure, elle prend une partie de la
 *            place sur le composant ; il faut donc en tenir compte dans le calcul de la taille maximale
 *            et dans l'organisation du composant parent : comment savoir la taille dont on dispose
 *            vraiment ou alors comment voir des infos sur la bordure ?
 *              ! Regarder getInsets() sur les composants
 *          * Gérer mieux le placement : suivant la taille du composant parent, ajuster la taille
 *            des composant pour lesquels il y a de la marge, en les agrandissant/rétrécissant
 *            proportionnellement à leur taille préférée (euh... seulement les rétrécir : il n'y a
 *            pas besoin de les agrandir puisqu'ils ont la place de prendre leur taille préférée)
 *          * Gérer les espaces et repoussoirs (struts et glue de Box)
 *          * Gérer les contraintes de taille : les composants ne devraient pas dépasser de leur conteneur
 *            (option pour gérer cette contrainte ; par défaut ça peut les positionner sans les contenir,
 *             mais il faut pouvoir leur imposer une taille)
 */
 
    
    // Constantes
    // (sens de placement)
    public static final int COLONNE = 1 ;
    public static final int LIGNE   = 2 ;
    // (alignements - note : des entiers longs surtout pour éviter que l'appel à Container.add ne choisisse la mauvaise méthode)
    public static final long ALIGN_CENTRE =  1 ;        // Centré
    public static final long ALIGN_GAUCHE =  2 ;        // A gauche (sens : colonne)
    public static final long ALIGN_DROITE =  4 ;        // A droite (sens : colonne)
    public static final long ALIGN_HAUT   =  8 ;        // En haut  (sens : ligne)
    public static final long ALIGN_BAS    = 16 ;        // En bas   (sens : ligne)

    
    // Données
    private int                 sensPlacement ;
    private long                alignementParDefaut ;   // Alignement par défaut des composants
    private Map<Component,Long> contraintes ;           // Contraintes sur les composants
    
    
    
    // Constructeur
    public PlaceurListe (int sensPlacement)
    {
        this (sensPlacement, ALIGN_GAUCHE + ALIGN_HAUT) ;
    }
    public PlaceurListe (int sensPlacement, long alignementParDefaut)
    {
        modifSensPlacement (sensPlacement) ;
        this.alignementParDefaut = alignementParDefaut ;
        this.contraintes         = new HashMap() ;
    }
    
    
    // Modifie le sens de placement eds composants
    public void modifSensPlacement (int sensPlacement)
    {
        this.sensPlacement = sensPlacement ;
    }
    

    
    // Mémorise les contraintes associées à un composant
    // Note : les contriaintes peuvent ne pas correspondre au sens. Par exemple on peut indiqué qu'un
    //   composant est aligné à gauche et en haut. Si l'organisation est en colonne, il sera aligné
    //   à gauche, si elle est en ligne il sera aligné en haut. Ce n'est pas grave (si on utilise une
    //   variable pour configurer un ensemble de valeurs) et ça peut servir si on moifie le sens du
    //   gestionnaire de composants
    public void addLayoutComponent (Component comp, Object contraintes)
    {
        if (contraintes != null)
            this.contraintes.put (comp, (Long)contraintes) ;
    }

    // Indique qu'un composant ne fait plus partie du conteneur
    // TODO A tester (tester surtout que ça s'exécute bien quand on retire un compoasnt et que ça
    //      fait bien ce qu'il faut)
    public void removeLayoutComponent (Component comp)
    {
        this.contraintes.remove (comp) ;
    }
    
    
    // Organiser les composants du conteneur parent
    // TODO A bien tester (et bien documenter quand ce sera terminé [doc de la classe])
    public void layoutContainer (Container parent)
    {
        // Composants en colonne
        if (this.sensPlacement == COLONNE)
        {
            int yCourant = 0 ;
            for (Component comp : parent.getComponents())
            {
                long contrainte = (this.contraintes.containsKey(comp) ? this.contraintes.get (comp) : this.alignementParDefaut) ;
                int xComp ;

                // Calculer la taille du composant
                comp.setSize (comp.getPreferredSize()) ;
                
                // Calculer l'alignement du composant
                // (par défaut à gauche (voir les notes : il se peut qu'on ait exprimé des contraintes que pour l'organsiation en ligne))
                if      ((contrainte & ALIGN_DROITE) != 0) xComp = parent.getWidth() - comp.getWidth() ;
                else if ((contrainte & ALIGN_CENTRE) != 0) xComp = (parent.getWidth() - comp.getWidth()) / 2 ;
                else                                       xComp = 0 ;
                
                // Positionner le composant
                comp.setLocation (xComp, yCourant) ;
                
                // Passer au suivant
                yCourant += comp.getHeight() ;
            }
        }
        // Composants en ligne
        else if (this.sensPlacement == LIGNE)
        {
            int xCourant = 0 ;
            for (Component comp : parent.getComponents())
            {
                long contrainte = (this.contraintes.containsKey(comp) ? this.contraintes.get (comp) : this.alignementParDefaut) ;
                int yComp ;
                
                // Calculer la taille du composant
                comp.setSize (comp.getPreferredSize()) ;

                // Calculer l'alignement du composant
                // (par défaut en haut (voir les notes : il se peut qu'on ait exprimé des contraintes que pour l'organsiation en ligne))
                if      ((contrainte & ALIGN_BAS)    != 0) yComp = parent.getHeight() - comp.getHeight() ;
                else if ((contrainte & ALIGN_CENTRE) != 0) yComp = (parent.getHeight() - comp.getHeight()) / 2 ;
                else                                       yComp = 0 ;
                
                // Positionner le composant
                comp.setLocation (xCourant, yComp) ;

                // Passer au suivant
                xCourant += comp.getWidth() ;
            }
        }
        else
        {
            exceptionSensInconnu() ;
        }
    }

    
    // Calculer la taille minimale du contenu du conteur parent
    // TODO A tester
    public Dimension minimumLayoutSize (Container parent)
    {
        int largeurMin = 0 ;
        int hauteurMin = 0 ;

        
        // Composants en colonne
        if (this.sensPlacement == COLONNE)
        {
            for (Component comp : parent.getComponents())
            {
                Dimension tailleComp = comp.getMinimumSize() ;
                largeurMin  = (int) Math.max (largeurMin, tailleComp.getWidth()) ;
                hauteurMin += tailleComp.getHeight() ;
            }
        }
        // Composants en ligne
        else if (this.sensPlacement == LIGNE)
        {
            for (Component comp : parent.getComponents())
            {
                Dimension tailleComp = comp.getMinimumSize() ;
                largeurMin += tailleComp.getWidth() ;
                hauteurMin  = (int) Math.max (hauteurMin, tailleComp.getHeight()) ;
            }
        }
        else
        {
            exceptionSensInconnu() ;
        }
        
        // Renvoyer la taille calculée
        return new Dimension (largeurMin, hauteurMin) ;
    }
    
    
    // Calculer la taille maximale du contenu du conteur parent
    // TODO Quand on gèrera des repoussoirs, penser à baser ça sur la taille du conteneur parent aussi
    // TODO A tester
    public Dimension maximumLayoutSize (Container parent)
    {
        int largeurMax = 0 ;
        int hauteurMax = 0 ;

        
        // Composants en colonne
        if (this.sensPlacement == COLONNE)
        {
            for (Component comp : parent.getComponents())
            {
                Dimension tailleComp = comp.getMaximumSize() ;
                largeurMax  = (int) Math.max (largeurMax, tailleComp.getWidth()) ;
                hauteurMax += tailleComp.getHeight() ;
            }
        }
        // Composants en ligne
        else if (this.sensPlacement == LIGNE)
        {
            for (Component comp : parent.getComponents())
            {
                Dimension tailleComp = comp.getMaximumSize() ;
                largeurMax += tailleComp.getWidth() ;
                hauteurMax  = (int) Math.max (hauteurMax, tailleComp.getHeight()) ;
            }
        }
        else
        {
            exceptionSensInconnu() ;
        }
        
        // Renvoyer la taille calculée
        return new Dimension (largeurMax, hauteurMax) ;
    }

    
    // Calculer la taille préférée du contenu du conteur parent
    // TODO A tester
    public Dimension preferredLayoutSize (Container parent)
    {
        int largeurPref = 0 ;
        int hauteurPref = 0 ;

        
        // Composants en colonne
        if (this.sensPlacement == COLONNE)
        {
            for (Component comp : parent.getComponents())
            {
                Dimension tailleComp = comp.getPreferredSize() ;
                largeurPref  = (int) Math.max (largeurPref, tailleComp.getWidth()) ;
                hauteurPref += tailleComp.getHeight() ;
            }
        }
        // Composants en ligne
        else if (this.sensPlacement == LIGNE)
        {
            for (Component comp : parent.getComponents())
            {
                Dimension tailleComp = comp.getPreferredSize() ;
                largeurPref += tailleComp.getWidth() ;
                hauteurPref  = (int) Math.max (hauteurPref, tailleComp.getHeight()) ;
            }
        }
        else
        {
            exceptionSensInconnu() ;
        }
        
        // Renvoyer la taille calculée
        return new Dimension (largeurPref, hauteurPref) ;
    }
    
    
    // Le conteneur de ce placeur souhaiterait être aligné à gauche et en haut
    // TODO ? : il faut donner une valeur... Il n'y a pas de raison que le gestionnaire de l'intérieur
    //          du composant sache ça... Est-ce qu'on peut le récupérer du conteneur lui-même ? Est-ce
    //          que je n'ai pas compris ce que ça faisait ?
    public float getLayoutAlignmentX (Container target)
    {
        return 0 ;
    }
    // Le conteneur de ce placeur souhaiterait être aligné à gauche
    public float getLayoutAlignmentY (Container target)
    {
        return 0 ;
    }

    
    
    // Lève une exception indiquant que le sens de placement n'est pas reconnu
    private void exceptionSensInconnu ()
    {
        throw new IllegalStateException ("Le sens de placement n'est pas reconnu : " + this.sensPlacement) ;
    }

    
    
    // Méthodes non implémentées
    // (ne rien faire : le placeur n'est pas intéressé par ces informations)
    public void addLayoutComponent (String name, Component comp)
    {}
    public void invalidateLayout (Container target)
    {}
    
}
