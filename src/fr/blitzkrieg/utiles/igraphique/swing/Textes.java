/**
 * 
 * Copyright 2009-2010 Sylvain Lavalley
 * 
 * This file is part of Blitzkrieg.
 *
 * Blitzkrieg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Blitzkrieg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blitzkrieg. If not, see <http://www.gnu.org/licenses/>.
 *  
 */
package fr.blitzkrieg.utiles.igraphique.swing;

import java.awt.Color ;

import javax.swing.JLabel ;



// Méthodes utiles pour la gestion des textes
public class Textes
{

    // Crée un texte sur une ligne en indiquant le texte à afficher et la couleur à lui donner
    public static JLabel creerTexte (String texte, Color couleur)
    {
        JLabel l_texte = new JLabel (texte) ;
        l_texte.setForeground (couleur) ;
        return l_texte ;
    }
    
}
