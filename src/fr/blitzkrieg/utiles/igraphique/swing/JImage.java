/**
 * 
 * Copyright 2009-2010 Sylvain Lavalley
 * 
 * This file is part of Blitzkrieg.
 *
 * Blitzkrieg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Blitzkrieg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blitzkrieg. If not, see <http://www.gnu.org/licenses/>.
 *  
 */
package fr.blitzkrieg.utiles.igraphique.swing;

import java.awt.Color ;
import java.awt.Dimension ;
import java.awt.Graphics ;
import java.awt.image.BufferedImage ;

import javax.swing.JComponent ;



// Composant affichant une image sur un fond de couleur donnée.
// Le composant prend la taille de l'image à afficher ou une taille fixe si on lui en a donné une.
// NOTE Il doit déjà y avoir ça en Java, mais je n'ai pas trouvé
public class JImage extends JComponent
{

    private Color         couleur ;             // Couleur à peindre sur le fond du panneau, s'il n'y a pas d'image
    private BufferedImage image ;               // Image à afficher sur le panneau
    private Dimension     tailleComposant ;     // Taile à donnre au composant
    
    
    
    // Constructeur
    // L'image et la taille du composant sont optionnelles
    public JImage (Color couleur, BufferedImage image, Dimension tailleComposant)
    {
        // Vérifier les paramètres
        if (couleur == null)
            throw new IllegalArgumentException ("La couleur doit être renseignée") ;
        
        // Stocker les valeurs des paramètres
        this.image           = image ;
        this.couleur         = couleur ;
        this.tailleComposant = tailleComposant ;
        
        // Déterminer la taille du composant
        ajusterTaille() ;
    }
    
    
    // Ajuste la taille du composant
    private void ajusterTaille ()
    {
        Dimension taille ;
        
        // Calculer la taille à donner au composant
        if (this.tailleComposant != null)
            taille = this.tailleComposant ;
        else if (this.image != null)
            taille = new Dimension (this.image.getWidth(), this.image.getHeight()) ;
        else
            taille = new Dimension (0, 0) ;
        
        // Indiquer au composant de prendre cette taille autant que possible
        this.setPreferredSize (taille) ;
        this.setMinimumSize   (taille) ;
        this.setMaximumSize   (taille) ;
    }
    
    
    // Modifie l'image affichée par le composant
    public void modifCouleur (Color couleur)
    {
        if (couleur == null)
            throw new IllegalArgumentException ("La couleur doit être renseignée") ;
        this.couleur = couleur ;
    }
    
    // Modifie l'image affichée par le composant
    public void modifImage (BufferedImage image)
    {
        this.image = image ;
        ajusterTaille() ;
    }
    
    // Modifie la taille à donner au composant
    public void modifTaille (Dimension taille)
    {
        this.tailleComposant = taille ;
        ajusterTaille() ;
    }

    
    
    // Peint le composant
    // TODO Ecrire sur un tampon réutilisé à chaque fois, pour éviter de consommer de la mémoire à chaque affichage
    //      (suivant la taille de l'image à afficher) ; ce tampon devrait aussi servir à réafficher
    //      directement le composant si la couleur de fond et l'image n'ont pas changé entre temps
    protected void paintComponent (Graphics g)
    {
        // Créer une image pour préparer le dessin
        BufferedImage img = new BufferedImage (this.getWidth(), this.getHeight(), BufferedImage.TYPE_INT_ARGB) ;
        Graphics gImg = img.getGraphics() ;
        
        // Peindre le fond du composant
        gImg.setColor (this.couleur) ;
        gImg.fillRect (0, 0, this.getWidth(), this.getHeight()) ;
        
        // Peindre l'image
        if (this.image != null)
            gImg.drawImage (this.image, 0, 0, this) ;
        
        // Peindre l'image sur le composant
        g.drawImage (img, 0, 0, this) ;
    }
    
    
}
