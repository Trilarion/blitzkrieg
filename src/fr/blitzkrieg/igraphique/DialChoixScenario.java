/**
 * 
 * Copyright 2009-2010 Sylvain Lavalley
 * 
 * This file is part of Blitzkrieg.
 *
 * Blitzkrieg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Blitzkrieg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blitzkrieg. If not, see <http://www.gnu.org/licenses/>.
 *  
 */
package fr.blitzkrieg.igraphique;

import java.awt.BorderLayout ;
import java.awt.FlowLayout ;
import java.awt.Frame ;
import java.awt.event.ActionEvent ;
import java.awt.event.ActionListener ;
import java.io.File ;
import java.util.ArrayList ;
import java.util.List ;

import javax.swing.JButton ;
import javax.swing.JDialog ;
import javax.swing.JList ;
import javax.swing.JPanel ;
import javax.swing.border.EtchedBorder ;
import javax.swing.event.ListSelectionEvent ;
import javax.swing.event.ListSelectionListener ;

import fr.blitzkrieg.utiles.igraphique.swing.Fenetres ;



// Boîte de dialogue permettant de choisir un scénario
public class DialChoixScenario extends JDialog implements ListSelectionListener, ActionListener
{

    private JList   liste_scenarios ;       // Liste des scénarios proposés
    private JButton bt_ok ;                 // Bouton Ok
    private JButton bt_annuler ;            // Bouton Annuler
    
    private String nomScenarioChoisi ;      // Nom du scénatio choisi (validé par le bouton Ok)
    
    
    
    // Constructeur
    public DialChoixScenario (Frame parent)
    {
        super (parent, "Choix d'un scénario", true) ;
        
        
        // Construire les composants
        this.liste_scenarios = new JList() ;
        this.liste_scenarios.setBorder (new EtchedBorder()) ;
        this.liste_scenarios.addListSelectionListener (this) ;
        this.bt_ok      = new JButton ("Ok") ;
        this.bt_ok.addActionListener (this) ;
        this.bt_annuler = new JButton ("Annuler") ;
        this.bt_annuler.addActionListener (this) ;
        
        // Assembler les composants
        this.getContentPane().setLayout (new BorderLayout()) ;
        this.getContentPane().add (this.liste_scenarios, BorderLayout.CENTER) ;
        JPanel p_bas = new JPanel (new FlowLayout (FlowLayout.CENTER)) ;
        p_bas.add (this.bt_ok) ;
        p_bas.add (this.bt_annuler) ;
        this.getContentPane().add (p_bas, BorderLayout.SOUTH) ;
        

        // Dimensionner et positionner la fenêtre
        // TODO Voir notes dans DialPoints
        this.setSize (300, 200) ;
        Fenetres.centrer (this, parent) ;
    }
    
    
    // Demande à la boîte de dialogue de charger les scénarios disponibles
    public void chargerScenarios ()
    {
        // TODO Arranger l'interface : ne pas utiliser une liste comme ça, mais par exemple des
        //      cases à cocher avec une liste pour la deuxième (et si on sélectionne un scénario dans la
        //      liste la case est automatiquement cochée par exemple) ; penser aussi qu'il y aura des
        //      campagnes plus tard et qu'il faut pouvoir afficher les descriptions
        // TODO Il faudrait au moins extraire leur nom et leur description (par exemple dans la
        //      classe StockagePartie, une nouvelle fonction qui ne fait que ça... note : ce sera peut-être
        //      long de décompresser et charger tous les scénarios à chaque nouvelle partie ; à voir, il
        //      faudra peut-être une option pour ne pas les recharger (ou seulement les nouveaux fichiers))
        //          ! Ca permet aussi de vérifier que ce sont de vrais scénarios et pas des fichiers qui
        //            traînaient par là par hasard
        
        // Lister les fichiers de scénarios
        // TODO Récupérer les classes de filtres qui récupèrent juste les fichiers correspondant à
        //      certains critères (par nom, ...)
        // TODO Constantes quelque part
        File repScenarios = new File ("donnees/scenarios/") ;
        File[] ficsScenarios = repScenarios.listFiles() ;
        List<String> nomsScenarios = new ArrayList() ;
        for (File fic : ficsScenarios)
        {
            if (fic.isFile() && fic.getName().endsWith (".scn"))
                nomsScenarios.add (fic.getName()) ;
        }
        
//        // Ajouter le scénario aléatoire
//        nomsScenarios.add (0, "Scénario aléatoire") ;
        
        // Remplir la liste
        this.liste_scenarios.setListData (nomsScenarios.toArray()) ;
        this.bt_ok.setEnabled (false) ;
        this.nomScenarioChoisi = null ;
    }
    
    
    // Rend visible ou invisible la boîte de dialogue
    // Si on rend visible la boîte de dialogue, la sélection de la liste est automatiquement effacée
    public void setVisible (boolean rendreVisible)
    {
        if (! this.isVisible() && rendreVisible)
            this.liste_scenarios.clearSelection() ;
        super.setVisible (rendreVisible) ;
    }
    
    
    // Renvoie le scénario sélectionné
    // Renvoie null si aucun n'a été sélectionné
    public String scenarioSelectionne ()
    {
        return this.nomScenarioChoisi ;
    }


    // Traitement du changement de sélection dans la liste
    public void valueChanged (ListSelectionEvent evt)
    {
        this.bt_ok.setEnabled (! this.liste_scenarios.isSelectionEmpty()) ;
    }
    
    
    // Traitement du clic sur les boutons
    public void actionPerformed (ActionEvent evt)
    {
        if (evt.getSource() == this.bt_ok)
            this.nomScenarioChoisi = (String) this.liste_scenarios.getSelectedValue() ;
        this.setVisible (false) ;
    }
    
}
