/**
 * 
 * Copyright 2009-2010 Sylvain Lavalley
 * 
 * This file is part of Blitzkrieg.
 *
 * Blitzkrieg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Blitzkrieg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blitzkrieg. If not, see <http://www.gnu.org/licenses/>.
 *  
 */
package fr.blitzkrieg.igraphique;

import java.awt.Color ;
import java.awt.event.MouseEvent ;
import java.awt.event.MouseListener ;
import java.awt.event.MouseMotionListener ;
import java.util.Observable ;
import java.util.Observer ;

import javax.swing.JLabel ;
import javax.swing.JPanel ;

import fr.blitzkrieg.Blitzkrieg ;
import fr.blitzkrieg.donnees.Case ;
import fr.blitzkrieg.donnees.Joueur ;
import fr.blitzkrieg.donnees.Partie ;
import fr.blitzkrieg.donnees.Terrain ;
import fr.blitzkrieg.traitements.Regles ;
import fr.blitzkrieg.utiles.igraphique.swing.Textes ;
import fr.blitzkrieg.utiles.igraphique.swing.gestplacement.PlaceurListe ;



// Affiche les informations sur les forces d'attaque et de défense d'une case
// NOTE Ce panneau s'inscrit comme "observateur" du panneau de terrain sous la forme d'un écouteur.
//      Créer une classe séparée d'écouteur n'est pas très intéressant ici : il ne s'agit pas d'une
//      action au sens de l'interface Action et la classe d'écouteur
//      (igraphique.ecouteursaffichage.EcouteurSourisTerrain) ne ferait que peu de choses et redirigerait
//      le traitement vers le panneau ; donc une version simploifiée est ici appliquée : le panneau
//      est lui-même écouteur des événements qui l'intéressent pour "observer" le composant graphique
//          => Noter ça dans la doc, c'est quelque chose qui resservira pour d'autres programmes
// TODO Tester ce que ça donne quand on réaffiche les données à chaque fois qu'on bouge la souris, sans
//      se préoccuper de savoir si c'est déjà affiché. Le code pour gérer ça est (légèrement) compliqué
//      et surtout étalé un peu partout. Donc si même sur une vieille machine ça ne change rien, il
//      vaudrait mieux le virer (à bien vérifier : le jeu devrait fonctionner sur des machines très modestes)
public class PanInfosAttaqueDefense extends JPanel implements MouseListener, MouseMotionListener, Observer
{

    private Partie  partie ;                // Partie observée
    private boolean partieModifiee ;        // Indique si la partie a été modifiée depuis la dernière mise-à-jour des données
    private boolean infosAffichees ;        // Indique si des infos sont attachées (si on affiche effectivement les infos sur une case)
    private int     xCaseInfosAffichees ;   // Case à propos de laquelle on affiche les infos
    private int     yCaseInfosAffichees ;
    
    private JLabel l_attaque ;              // Valeurs à afficher
    private JLabel l_defense ;
    private JLabel l_reussite ;
    
    
    
    // Constructeur
    // TODO Ici et dans les autres panneaux, il faudrait s'inquiéter du fait que la partie peut
    //      ne pas avoir été fournie (je crois que c'est le cas ailleurs, bien vérifier partout)
    public PanInfosAttaqueDefense ()
    {
        // CODE Code à arranger
        this.l_attaque  = Textes.creerTexte ("", new Color (0xF0E68C)) ;
        this.l_defense  = Textes.creerTexte ("", new Color (0xF0E68C)) ;
        this.l_reussite = Textes.creerTexte ("", new Color (0xF0E68C)) ;
        afficherInfosVides() ;

        JPanel p_attaque  = new JPanel (new PlaceurListe (PlaceurListe.LIGNE, PlaceurListe.ALIGN_BAS)) ;
        JPanel p_defense  = new JPanel (new PlaceurListe (PlaceurListe.LIGNE, PlaceurListe.ALIGN_BAS)) ;
        JPanel p_reussite = new JPanel (new PlaceurListe (PlaceurListe.LIGNE, PlaceurListe.ALIGN_BAS)) ;
        p_attaque.setBackground  (Color.DARK_GRAY) ;
        p_defense.setBackground  (Color.DARK_GRAY) ;
        p_reussite.setBackground (Color.DARK_GRAY) ;
        p_attaque.add  (Textes.creerTexte ("Attaque : ",  new Color (0xF0E68C))) ;
        p_defense.add  (Textes.creerTexte ("Défense : ",  new Color (0xF0E68C))) ;
        p_reussite.add (Textes.creerTexte ("Réussite : ", new Color (0xF0E68C))) ;
        p_attaque.add  (this.l_attaque) ;
        p_defense.add  (this.l_defense) ;
        p_reussite.add (this.l_reussite) ;
        
        this.setLayout (new PlaceurListe (PlaceurListe.COLONNE)) ;
        this.add (p_attaque) ;
        this.add (p_defense) ;
        this.add (p_reussite) ;
        
        // TODO Unifier cette constante (utilisée à plein d'endroits)
        this.setBackground (Color.DARK_GRAY) ;
    }
    
    
    
    // Modifie la parie à afficher
    public void modifPartieAAfficher (Partie partie)
    {
        // Regarder la nouvelle partie
        if (this.partie != null)
            this.partie.deleteObserver (this) ;
        this.partie         = partie ;
        if (this.partie != null)
            this.partie.addObserver (this) ;
        
        // Initialiser l'affichage
        this.partieModifiee = true ;
        afficherInfosVides() ;
    }
    
    
    // Noter quand la partie est moifiée
    // TODO Essayer de détecter la fin du tour d'un joueur pour réafficher des infos vides (plutôt que
    //      conserver les infos du joueur précédent)
    public void update (Observable o, Object arg)
    {
        if (this.partie.terminee())
            modifPartieAAfficher (null) ;
        this.partieModifiee = true ;
        if (this.partie == null || ! this.partie.commencee() || this.partie.observationSuspendue())
            afficherInfosVides() ;
        else if (this.infosAffichees)
            afficherInfos (this.xCaseInfosAffichees, this.yCaseInfosAffichees) ;
    }
    
    
    // Gestion des réaffichages lorsque la souris parcours le terrain
    // CODE Eventuellement arranger et surtout commenter ce code
    public void mouseMoved (MouseEvent evt)
    {
        if (this.partie != null && this.partie.commencee())
        {
            Terrain terrain = this.partie.terrain() ;
            int xCase = (int) Math.floor ((double) evt.getX() / PanTerrain.TAILLE_CASE) ;
            int yCase = (int) Math.floor ((double) evt.getY() / PanTerrain.TAILLE_CASE) ;
            if (xCase < terrain.largeur() && yCase < terrain.hauteur())
            {
                Joueur joueurCourant = this.partie.joueurCourant() ;
                Case   casePointee   = terrain.caseEn (xCase, yCase) ;
                if (casePointee.decouverte (joueurCourant))
                {
                    if (this.partieModifiee || ! (xCase == this.xCaseInfosAffichees &&
                                                  yCase == this.yCaseInfosAffichees))
                    {
                        afficherInfos (xCase, yCase) ;
                    }
                }
                else
                {
                    afficherInfosVides() ;
                }
            }
            else
            {
                afficherInfosVides() ;
            }
        }
    }
    
    
    // La souris quitte le terrain (la carte principale)
    public void mouseExited (MouseEvent evt)
    {
        if (this.infosAffichees)
            afficherInfosVides() ;
    }
    
    
    // Affiche des informations vides concernant la probabilité de réussir une attaque sur la case pointée
    //   (quand le souris ne pointe pas de case par exemple)
    private void afficherInfosVides ()
    {
        // Vider les infos
        this.l_attaque.setText  ("--") ;
        this.l_defense.setText  ("--") ;
        this.l_reussite.setText ("--") ;
        
        // Pas de modification de la partie depuis le dernier affichage (qui vient de se produire)
        this.partieModifiee      = false ;
        this.infosAffichees      = false ;
        this.xCaseInfosAffichees = -1 ;
        this.yCaseInfosAffichees = -1 ;
    }
    
    
    // Affiche les informations sur la probabilité de réussir une attaque sur la case dont les coordonnées
    //   sont fournies en paramètre
    private void afficherInfos (int x, int y)
    {
        Joueur  joueurCourant ;         // Joueur courant
        Regles  regles ;                // Règles de la partie en cours
        Double forceAttaque ;           // Valeurs à afficher
        Double forceDefense ;
        Double probaReussite ;
        
        
        // Initialisations
        joueurCourant = this.partie.joueurCourant() ;
        regles        = Blitzkrieg.instance().reglesCourantes() ;
        
        // Calculer les valeurs à afficher
        forceAttaque  = regles.forceAttaque         (joueurCourant, x, y) ;
        forceDefense  = regles.forceDefense         (x, y) ;
        probaReussite = regles.probaReussiteAttaque (joueurCourant, x, y) ;
        
        // Afficher les infos
        this.l_attaque.setText  (forceAttaque  == null ? "--" : "" + (int) Math.round (forceAttaque)) ;
        this.l_defense.setText  (forceDefense  == null ? "--" : "" + (int) Math.round (forceDefense)) ;
        this.l_reussite.setText (probaReussite == null ? "--" : "" + (int) Math.round (probaReussite * 100) + "%") ;
        
        // Pas de modification de la partie depuis le dernier affichage (qui vient de se produire)
        this.partieModifiee      = false ;
        this.infosAffichees      = true ;
        this.xCaseInfosAffichees = x ;
        this.yCaseInfosAffichees = y ;
    }
    
    
    
    // Méthodes non implémentées de MouseListener
    public void mouseClicked  (MouseEvent e) {}
    public void mouseEntered  (MouseEvent e) {}
    public void mousePressed  (MouseEvent e) {}
    public void mouseReleased (MouseEvent e) {}
    
    // Méthodes non implémentées de MouseMotionListener
    public void mouseDragged (MouseEvent e) {}
    
}
