/**
 * 
 * Copyright 2009-2010 Sylvain Lavalley
 * 
 * This file is part of Blitzkrieg.
 *
 * Blitzkrieg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Blitzkrieg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blitzkrieg. If not, see <http://www.gnu.org/licenses/>.
 *  
 */
package fr.blitzkrieg.igraphique.actions;

import java.awt.event.ActionEvent ;

import javax.swing.AbstractAction ;

import fr.blitzkrieg.Blitzkrieg ;
import fr.blitzkrieg.igraphique.DialInfosJoueurs ;



// Affichage des infos sur les joueurs
public class ActionAfficherInfosJoueurs extends AbstractAction
{

    // Constructeur
    public ActionAfficherInfosJoueurs ()
    {
        super ("Informations joueurs") ;
    }
    
    
    // Exécuter l'action
    public void actionPerformed (ActionEvent evt)
    {
        DialInfosJoueurs dialInfosJoueurs = Blitzkrieg.instance().fenPrincipale().dialInfosJoueurs() ;
        if (dialInfosJoueurs != null)
            dialInfosJoueurs.setVisible (true) ;
    }

}
