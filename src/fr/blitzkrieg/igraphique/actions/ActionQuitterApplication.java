/**
 * 
 * Copyright 2009-2010 Sylvain Lavalley
 * 
 * This file is part of Blitzkrieg.
 *
 * Blitzkrieg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Blitzkrieg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blitzkrieg. If not, see <http://www.gnu.org/licenses/>.
 *  
 */
package fr.blitzkrieg.igraphique.actions;

import java.awt.event.ActionEvent ;

import javax.swing.AbstractAction ;
import javax.swing.JOptionPane ;

import fr.blitzkrieg.Blitzkrieg ;
import fr.blitzkrieg.donnees.Partie ;



// Fermeture de l'application
public class ActionQuitterApplication extends AbstractAction
{

    // Constructeur
    public ActionQuitterApplication ()
    {
        super ("Quitter") ;
    }
    
    
    // Exécuter l'action
    public void actionPerformed (ActionEvent evt)
    {
        Blitzkrieg application ;                // Instance de l'application
        Partie     partie ;                     // Partie courante

        
        // Initialisations
        application = Blitzkrieg.instance() ;
        partie = application.partieCourante() ;

        // Demander confirmation s'il y a une partie en cours
        if (partie == null ||
            JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog (application.fenPrincipale(), "Quitter Blitzkrieg ?", "Quitter le jeu", JOptionPane.YES_NO_OPTION))
        {
            System.exit (0) ;
        }
    }

}
