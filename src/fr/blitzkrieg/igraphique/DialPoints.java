/**
 * 
 * Copyright 2009-2010 Sylvain Lavalley
 * 
 * This file is part of Blitzkrieg.
 *
 * Blitzkrieg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Blitzkrieg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blitzkrieg. If not, see <http://www.gnu.org/licenses/>.
 *  
 */
package fr.blitzkrieg.igraphique;

import java.awt.BorderLayout ;
import java.awt.Frame ;
import java.util.ArrayList ;
import java.util.HashMap ;
import java.util.List ;
import java.util.Map ;

import javax.swing.JDialog ;
import javax.swing.JLabel ;
import javax.swing.JPanel ;

import fr.blitzkrieg.donnees.Joueur ;
import fr.blitzkrieg.donnees.Partie ;
import fr.blitzkrieg.utiles.igraphique.swing.Fenetres ;
import fr.blitzkrieg.utiles.igraphique.swing.gestplacement.PlaceurListe ;



// Boîte de dialogue affichant les points des différents joueurs
public class DialPoints extends JDialog
{

    private Partie partie ;         // Partie concernée

    // CODE Renommer les l_... en txt_... ; et pareil pour les autres composants : préfixes en français (plus clairs)
    private JLabel l_texteVainqueur ;                 // Affichage du vainqueur
    private Map<Joueur,JLabel> textesJoueurs ;      // Composants affichant les points des joueurs

    
    
    // Constructeur
    // TODO S'occuper du titre des fenêtres et boîtes de dialogue
    public DialPoints (Frame parent, Partie partie)
    {
        super (parent, "Points de victoire", true) ;
        
        
        // Stocker la partie
        this.partie = partie ;
        
        // Créer les objets pour l'affichage des informations
        // TODO Un truc plus subtil, du genre une table d'association joueur -> texte
        this.l_texteVainqueur = new JLabel (" ") ;
        this.textesJoueurs  = new HashMap() ;
        JPanel p_aff = new JPanel (new PlaceurListe (PlaceurListe.COLONNE)) ;
        p_aff.add (this.l_texteVainqueur) ;
        for (Joueur joueur : partie.joueurs())
        {
            JLabel txt_joueur = new JLabel (" ") ;
            p_aff.add (txt_joueur) ;
            this.textesJoueurs.put (joueur, txt_joueur) ;
        }
        
        // Assembler les composants
        this.getContentPane().setLayout (new BorderLayout()) ;
        this.getContentPane().add (p_aff,                     BorderLayout.CENTER) ;
        this.getContentPane().add (Fenetres.panneauOk (this), BorderLayout.SOUTH) ;
        
        // Autres actions sur la fenêtre
        Fenetres.ajouterEcouteurEchapPourMasquer  (this) ;
        Fenetres.ajouterEcouteurEntreePourMasquer (this) ;

        // Dimensionner et positionner la fenêtre
        // TODO Déterminer la taille, éventuellement en donnant une limite par rapport aux noms
        //      des joueurs et en réservant de la place pour les scores
        this.setSize (300, 200) ;
        Fenetres.centrer (this, parent) ;
    }
    
    
    
    // Affiche la boîte de dialogue pour l'affichage des scores courants de la partie donnée
    public void afficherScoresCourants ()
    {
        affScores (false) ;
    }
    
    
    // Affiche la boîte de dialogue pour l'affichage des scores de fin de partie et du vainqueur
    public void afficherScoresEtVainqueur ()
    {
        affScores (true) ;
    }
    
    
    // Affiche les scores courants et le nom du vainqueur si demandé
    private void affScores (boolean affVainqueur)
    {
        // Afficher le nom du vainqueur si demandé
        if (affVainqueur)
        {
            // Déterminer leplus grand nombre de points
            int nbPointsMax = 0 ;
            for (Joueur joueur : this.partie.joueurs())
                nbPointsMax = Math.max (nbPointsMax, joueur.nbPointsVictoire()) ;
            // Lister les vainqueurs
            // TODO Un truc plus subtil en cas d'égalité de tout le monde ou d'une partie des joueurs
            List<Joueur> vainqueurs = new ArrayList() ;
            for (Joueur joueur : this.partie.joueurs())
            {
                if (joueur.nbPointsVictoire() == nbPointsMax)
                    vainqueurs.add (joueur) ;
            }
            // Afficher les vainqueurs
            // TODO Un truc pour quand ça dépasse (pour indiquer qu'il y en a d'autres : "..." -> + infobulle)
            String txtNomsVainqueurs = "" ;
            for (Joueur joueur : vainqueurs)
            {
                if (! txtNomsVainqueurs.isEmpty())
                    txtNomsVainqueurs += ", " ;
                txtNomsVainqueurs += joueur.nom() ;
            }
            this.l_texteVainqueur.setText        ("Vainqueur : " + txtNomsVainqueurs) ;     // TODO A accorder au pluriel (deux mots différents à cause de l'internationalisation)
            this.l_texteVainqueur.setToolTipText (this.l_texteVainqueur.getText()) ;
        }
        else
        {
            this.l_texteVainqueur.setText        (" ") ;
            this.l_texteVainqueur.setToolTipText (this.l_texteVainqueur.getText()) ;
        }
        
        // Afficher les scores actuels des joueurs
        for (Joueur joueur : this.partie.joueurs())
            this.textesJoueurs.get(joueur).setText (joueur.nom() + " : " + joueur.nbPointsVictoire()) ;
        
        // Afficher la boîte de dialogue
        setVisible (true) ;
    }
    
    
// NOTE Appelé quand on clique sur la croix pour la fermer ; voir si on peut faire quelque chose de simple
//      pour empêcher l'appel de cette méthode depuis n'importe où
//      
//    // La méthode setVisible ne devrait pas être appelée (appeler la méthode de la classe mère si besoin
//    //   en interne)
//    // TODO A tester
//    public void setVisible (boolean visible)
//    {
//        throw new IllegalStateException ("La méthode setVisible de cette classe ne devrait pas être appelée (pour les appels depuis la classe elle-même, appeler super.setVisible (...))") ;
//    }
    
}
