/**
 * 
 * Copyright 2009-2010 Sylvain Lavalley
 * 
 * This file is part of Blitzkrieg.
 *
 * Blitzkrieg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Blitzkrieg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blitzkrieg. If not, see <http://www.gnu.org/licenses/>.
 *  
 */
package fr.blitzkrieg.igraphique;

import java.awt.BorderLayout ;

import javax.swing.JFrame ;

import fr.blitzkrieg.donnees.Partie ;



// Affiche une mini-carte représentant le terrain de la partie courante
// NOTE Problèmes pour maintenir la fenêtre au-dessus de l'autre
//         !!! Conflit avec la boîte de dialogue modale qui apparaît (Fluxbox 2009-11) ; apparemment elle prend la place
//             de "seule fenêtre au-dessus de tout le monde" ; et si on le redis pour la mini-carte, ça chasse
//             la boîte de dialogue
//                  ! Ca peut être corrigé plus délicatement avec toFront(), mais le comportement redevient
//                    normal (pas toujours dessus) dès qu'on ferme la boîte de dialogue modale
//                  ! Une solution c'est de détecter les fermetures de boîtes de dialogues modales pour réimposer
//                    la propriété à la fenêtre de la mini-carte, mais c'est lourd ; à garder sous la main comme
//                    idée (derrière l'oreille plutôt, non ?), mais en attendant un simple panneau dans
//                    l'interface principale est préférable
//                  ! Jeter un oeil sur JFrame.setLayerdPane (...)
//          ! La fenêtre reste ici pour l'instant, mais elle ne sert plus
//              ! Il restait un problème : la fenêtre ne prenait pas la taille du panneau quand il
//                changeait de taille (ni au début d'ailleurs, mais par hasard c'était la bonne taille)
public class FenMiniCarte extends JFrame
{
    
    private PanMiniCarte p_dessin ;         // Panneau de dessin de la carte
    
    
    
    // Constructeur
    public FenMiniCarte ()
    {
        super ("Mini-carte") ;

        // Construire et assembler les composants
//this.p_dessin = new PanMiniCarte (this) ;
        this.p_dessin = new PanMiniCarte() ;
        this.getContentPane().setLayout (new BorderLayout()) ;
        this.getContentPane().add (this.p_dessin) ;
        
        // Choisir une taille arbitraire tant qu'il n'y a rien d'affiché dedans
        // (cette fenêtre n'est pas redimensionnable à la main)
        this.setAlwaysOnTop (true) ;
        this.setResizable (false) ;
        this.pack() ;
//System.out.println ("Toujours dessus supporté : " + this.isAlwaysOnTopSupported()) ;
//System.out.println ("Toujours dessus : " + this.isAlwaysOnTop()) ;
    }
    
    
    // Modifie la partie à afficher
    public void modifPartieAAfficher (Partie partie)
    {
        // Transférer l'information au panneau
        this.p_dessin.modifPartieAAfficher (partie) ;
        
        // Redimensionner la fenêtre
        this.pack() ;
    }
    
}
