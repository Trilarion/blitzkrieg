/**
 * 
 * Copyright 2009-2010 Sylvain Lavalley
 * 
 * This file is part of Blitzkrieg.
 *
 * Blitzkrieg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Blitzkrieg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blitzkrieg. If not, see <http://www.gnu.org/licenses/>.
 *  
 */
package fr.blitzkrieg.donnees;

import static fr.blitzkrieg.donnees.Case.TypeCase.CASE_MER ;
import static fr.blitzkrieg.donnees.Case.TypeCase.CASE_VILLE ;

import java.util.ArrayList ;
import java.util.Collection ;
import java.util.HashMap ;
import java.util.Iterator ;
import java.util.List ;
import java.util.Map ;
import java.util.Random ;

import fr.blitzkrieg.Blitzkrieg ;
import fr.blitzkrieg.donnees.Case.TypeCase ;



// Terrain de jeu
// Rassemble les données définissant le terrain et les données propres à une partie.
// TODO Voir plus tard si on sépare ça en deux, mais c'est aussi moche de stocker par exemple l'état des
//      cases en dehors ; et pour la visibilité des cases pour chaque joueur il n'y a pas de bon endroit
//      (ni pour les troupes)
//          => A voir quand on sauvegardera des parties/scénarios/cartes, si c'est mieux de regrouper
//             certaines données
// DOC Modèle Observateur/Observé - deux solutions : soit la plupart des classes de données sont
//     observables et les objets de l'interface graphique s'inscrivent à tous ceux qui les intéressent
//     (pas exemple pas mal d'opérations sont codées dans les cases et appelées directement depuis
//      les contrôleurs [est-ce bien ?]), soit les objets de données font remonter les notifications
//      jusqu'à la aprtie (par exemple) ; dans la deuxième solution il n'y a pas besoin que tous les
//      objets de données soient observables, mais ils doivent quand même avoir une méthode pour faire
//      remonter les notifications et un lien vers l'objet "parent", ce qui rend la construction du
//      graphe de données plus complexe ; donc on opte pour la première solution, un peu plus complexe
//      pour les observateurs, qui doivent s'inscrire et se désinscrire à plus d'objets ; il y avait une
//      troisième solution : les règles sont codées à plus haut niveau (par exemple dans la partie) et
//      les objets de données ne sont que des conteneurs qui acceptent n'importe quelle modification
//      (ce qui est moins bien du point de vue de la cohérence des données) [on pourrait quand même
//      coder les règles dans la partie et relayer les actions vers les classes plus bas, dans le seul
//      but de s'assurer que toute modiifcation passe par la partie, qui est observable, mais ça oblige
//      à écrire une méthode pour chaque actgion du jeu dans la partie en plus d'un contrôleur et
//      des méthodes dans les classes plus bas (cases notamment) ; donc on reste pour l'instant sur
//      la première solution ; en en fait le plus simple pour enregistrer les objets de l'interface
//      graphique à toutes les classes de la partie c'est que la méthode d'inscription comme observateur
//      de la partie inscrive automatiquement aux autres classes (ce qui laisse l'interface graphique en
//      dehors des subtilités de "est-ce que je m'inscris à plein de classes ou est-ce que les
//      notifications remontent ?" [à part qu'ils reçoivent des notifications de divers objets]) ;
//      la troisième solution a un intérêt si on cherche activement à rassembler toutes les règles
//      quelque part plutôt que les laisser éparpillées dans les contrôleurs et les classes de données
//      => à réfléchir, au moins pour voir ce que ça pourrait donner, comme exemple pour d'autres jeux :
//      ce serait ma couche de traitements, mais pas facile de transférer ce qui se trouve dans les
//      contrôleurs et ce qui se trouve dans les cases (?, si même c'est une bonne idée) vers les
//      traitements (ben c'est moins bien du point de vue des classes individuelles, qui n'assurent plus
//      leur cohérenced (ou moins), mais cette cohérence est garantie par le rassemblement des
//      règles dans les classes de traitement) ; tiens, d'aileurs si on met les règles dans les classes
//      de traitement on ne passe pas automatiquement par la partie et on a toujours le problème des
//      notifications => c'est un autre problème que cette gestion des règles ; tiens dans la première
//      solution les observateurs se retrouvent avec n'importe qui qui leur envoie une notification
//      et ça rend les transtypages hasardeux (ou pénibles à écrire) : les observateurs ont intérêt
//      à savoir ce qu'ils observent (et à ne pas avoir plusieurs choses différentes à observer ; ici
//      ils s'en fichent : quand ils reçoivent une notification ils regardent toute la partie qu'ils
//      observent pour se réafficher complètement)]
//          => Essayer de faire ce rassemblement des règles, voir si ça ne supprime pas trop de
//             choses des classes de données (par exemple est-ce que la partie doit toujours déterminer
//             elle-même si elle est terminée ? [par exemple quand il n'y a plus de joueurs])
//          => Les contrôleurs doivent en faire le moins possible (par exemple le clic sur le terrain détermine
//             dans quel cas on est : retrait de troupe (case au joueur et troupe présente), dépôt de troupe
//             (...) ou attaque (...), pas si c'est vraiment autorisé par les règles (on ne peut pas retirer
//             une troupe d'une case morte, ...) : c'est comme s'il y avait plusieurs boutons pour déposer,
//             retirer et attaquer)
public class Terrain
{

    private int      largeur ;          // Taille du terrain
    private int      hauteur ;
    private Case[][] cases ;            // Cases constituant le terrain
    
    private List<Mer>     mers ;        // Mers/lacs contenues dans le terrain
    private Map<Case,Mer> cotes ;       // Association case de côte -> mer  // ENCOURS Merde ! Il devrait pouvoir y avoir plusieurs mers associés à une case de terre
    
    
    
    // Constructeur
    // TODO A supprimer
    public Terrain ()
    {
// Terrain bidon en dur pour les tests
this.largeur = 100 ;
this.hauteur = 100 ;
this.cases = new Case[this.largeur][this.hauteur] ;
// (créer le terrain)
Random genAlea = Blitzkrieg.instance().genAlea() ;
int probaVilles = 3 ;
int probaMers   = 15 ;
for (int x = 0 ; x < this.largeur ; x++)
for (int y = 0 ; y < this.hauteur ; y++)
{
    // (type de terrain)
    TypeCase type ;
    int nbAlea = genAlea.nextInt (100) ;
    if (nbAlea < probaMers)
        type = TypeCase.CASE_MER ;
    else if (nbAlea < probaMers + probaVilles)
        type = TypeCase.CASE_VILLE ;
    else
        type = TypeCase.CASE_TERRE ;
//    if (! ((1 <= x && x < this.largeur-1) && (2 <= y && y < this.hauteur)))
//        type = TypeCase.CASE_MER ;
//    else if ((x == 8 && y == 3) || (x == 3 && y == 8) || (x == 5 && y == 4) || (x == 6 && y == 3))
//        type = TypeCase.CASE_VILLE ;
//    else
//        type = TypeCase.CASE_TERRE ;
    this.cases[x][y] = new Case (x, y, type, null, false) ;
    
//    // (troupes)
//    if ((x == 3 && y == 3) || (x == 4 && y == 4) || (x == 5 && y == 5) || (x == 6 && y == 6))
//        this.cases[x][y].deposerTroupe() ;
    
//    // (cases mortes)
//    if ((x == 3 && y == 3) || (x == 4 && y == 3) || (x == 3 && y == 4) || (x == 4 && y == 4))
//        this.cases[x][y].mourir() ;
}

        initialiserMersEtCotes() ;
    }
    
    
    // Constructeur
    // CODE Vérifier que le tableau correspond bien aux dimensions indiquées (fonctions dans VerifParams)
    public Terrain (int largeur, int hauteur, Case[][] cases)
    {
        // Mémoriser les paramètres
        this.largeur = largeur ;
        this.hauteur = hauteur ;
        this.cases   = cases ;
        
        // Calculer les autres attributs
        initialiserMersEtCotes() ;
    }
    
    
    
    // Remplit les objets Mer associés au terrain
    private void initialiserMersEtCotes ()
    {
        // Numéroter les terres et les mers
        // TODO Optimisation pour faire le parcours dans les deux sens ? Ca dépend du temps que ça prend
        int[][] numsTerres = new int[this.largeur][this.hauteur] ;
        int[][] numsMers   = new int[this.largeur][this.hauteur] ;
        int cptTerres = 0 ;
        int cptMers   = 0 ;
        boolean modifieNumsTerres ;
        do
        {
            modifieNumsTerres = false ;
            for (int x = 0 ; x < this.largeur ; x++)
            for (int y = 0 ; y < this.hauteur ; y++)
            {
                if (this.cases[x][y].type() == TypeCase.CASE_TERRE || this.cases[x][y].type() == TypeCase.CASE_VILLE)
                {
                    List<Case> casesVoisines = casesVoisines (x, y) ;
                    Iterator<Case> itCasesVoisines = casesVoisines.iterator() ;
                    while (itCasesVoisines.hasNext())
                    {
                        TypeCase type = itCasesVoisines.next().type() ;
                        if (! (type == TypeCase.CASE_TERRE || type == TypeCase.CASE_VILLE))
                            itCasesVoisines.remove() ;
                    }
                    int numTerreMax = numsTerres[x][y] ;
                    for (Case caseVoisine : casesVoisines)
                        numTerreMax = Math.max (numTerreMax, numsTerres[caseVoisine.x()][caseVoisine.y()]) ;
                    if (numTerreMax > numsTerres[x][y])
                    {
                        numsTerres[x][y]  = numTerreMax ;
                        modifieNumsTerres = true ;
                    }
                    else if (numTerreMax == 0)
                    {
                        numsTerres[x][y]  = ++cptTerres ;
                        modifieNumsTerres = true ;
                    }
                }
            }
            
        } while (modifieNumsTerres) ;
        
        boolean modifieNumsMers ;
        do
        {
            modifieNumsMers = false ;
            for (int x = 0 ; x < this.largeur ; x++)
            for (int y = 0 ; y < this.hauteur ; y++)
            {
                if (this.cases[x][y].type() == TypeCase.CASE_MER)
                {
                    List<Case> casesVoisines = casesVoisines (x, y, TypeCase.CASE_MER) ;
                    int numMerMax = numsMers[x][y] ;
                    for (Case caseVoisine : casesVoisines)
                        numMerMax = Math.max (numMerMax, numsMers[caseVoisine.x()][caseVoisine.y()]) ;
                    if (numMerMax > numsMers[x][y])
                    {
                        numsMers[x][y]  = numMerMax ;
                        modifieNumsMers = true ;
                    }
                    else if (numMerMax == 0)
                    {
                        numsMers[x][y]  = ++cptMers ;
                        modifieNumsMers = true ;
                    }
                }
            }
            
        } while (modifieNumsMers) ;
        
        // Pour chaque mer, récupérer la liste des cases de côte
        List<Mer> mers = new ArrayList() ;
        for (int iMer = 1 ; iMer <= cptMers ; iMer++)
        {
            // Préparer les listes de cases de côtes
            List<List<Case>> casesCotes = new ArrayList() ;
            for (int iCote = 0 ; iCote < cptTerres ; iCote++)
                casesCotes.add (new ArrayList()) ;
            // Parcourir la carte
            for (int x = 0 ; x < this.largeur ; x++)
            for (int y = 0 ; y < this.hauteur ; y++)
            {
                // Si c'est une case de la mer courante, ajouter les cases de côtes aux listes
                if (this.cases[x][y].type() == TypeCase.CASE_MER && numsMers[x][y] == iMer)
                {
                    for (Case caseVoisine : casesVoisines (x, y))
                    {
                        if (caseVoisine.type() == TypeCase.CASE_TERRE || caseVoisine.type() == TypeCase.CASE_VILLE)
                        {
                            int numTerre = numsTerres[caseVoisine.x()][caseVoisine.y()] ;
                            if (! casesCotes.get(numTerre-1).contains (caseVoisine))
                                casesCotes.get(numTerre-1).add (caseVoisine) ;
                        }
                    }
                }
            }
        
            // Supprimer les listes vides
            Iterator<List<Case>> itCotes = casesCotes.iterator() ;
            while (itCotes.hasNext())
            {
                if (itCotes.next().isEmpty())
                    itCotes.remove() ;
            }

            // Créer la mer s'il y a des côtes (=> supprime les mers vides [numéro remplacés par d'autres])
            //  CODE Il faudra arranger tout ce bazar ; et le tester sérieusement un jour...
            // TODO Ca sert à quelque chose ce numéro de mer ?
            if (casesCotes.size() > 0)
                mers.add (new Mer (mers.size(), casesCotes)) ;
        }
        
//System.out.println ("Compteur  mers : " + cptMers) ;
//System.out.println ("Nombre de mers : " + mers.size()) ;
        
        // Calculer les associations case côte -> mer
        Map<Case,Mer> cotesMers = new HashMap() ;
        for (Mer mer : mers)
        {
            for (Case caseCote : mer.cotes())
                cotesMers.put (caseCote, mer) ;
        }
        
        // Mémoriser les informations dans les attributs de l'objet
        this.mers  = mers ;
        this.cotes = cotesMers ;
        
//// (pour vérifier, compter les terres différentes)
//int[] nbCases = new int[cptTerres] ;
//for (int x = 0 ; x < this.largeur ; x++)
//for (int y = 0 ; y < this.hauteur ; y++)
//{
//    if (numsTerres[x][y] != 0)
//        nbCases[numsTerres[x][y]-1]++ ;
//}
//System.out.println ("Terres : " + Arrays.toString (nbCases)) ;
//nbCases = new int[cptMers] ;
//for (int x = 0 ; x < this.largeur ; x++)
//for (int y = 0 ; y < this.hauteur ; y++)
//{
//    if (numsMers[x][y] != 0)
//        nbCases[numsMers[x][y]-1]++ ;
//}
//System.out.println ("Mers : " + Arrays.toString (nbCases)) ;
    }
    
        
    
    // Renvoie la largeur du terrain
    public int largeur ()
    {
        return this.largeur ;
    }

    
    // Renvoie la hauteur du terrain
    public int hauteur ()
    {
        return this.hauteur ;
    }

    
    // Renvoie les mers contenues dans le terrain
    public List<Mer> mers ()
    {
        return this.mers ;
    }

    
    // Renvoie la case aux coordonnées demandées
    public Case caseEn (int x, int y)
    {
        return this.cases[x][y] ;
    }
    

    
    // Rend la case de coordonnées données ainsi que ses voisines visibles pour le joueur en paramètre
    public void regarder (Joueur joueur, int x, int y)
    {
        List<Case> cases ;          // Cases à rendre visible au joueur

        
        // Rassembler la liste des cases (y compris la case centrale, utile dans cretains cas)
        cases = casesVoisines (x, y) ;
        cases.add (caseEn (x, y)) ;
        
        // Rendre les cases visibles
        for (Case caseCourante : cases)
            caseCourante.decouvrir (joueur) ;
    }
    
    
    // Calcule la visibilité de chaque joueur.
    // Les cases visibles actuellement par chaque joueur sont marquées, les cases déjà visibles ne sont
    //   pas touchées
    public void mettreAJourVisibiliteJoueurs ()
    {
        // Parcourir les cases
        for (int x = 0 ; x < this.largeur ; x++)
        for (int y = 0 ; y < this.hauteur ; y++)
        {
            mettreAJourVisibiliteProprietaire (x, y) ;
        }
    }
    
    // Augmente la visibilité d'un joueur autour d'une case donnée
    // Le propriétaire de la case découvre la case et les cases autour
    public void mettreAJourVisibiliteProprietaire (int x, int y)
    {
        Case caseCentrale = this.caseEn (x, y) ;
        if (caseCentrale.proprietaire() != null)
        {
            caseCentrale.decouvrir (caseCentrale.proprietaire()) ;
            for (Case c : casesVoisines (x, y))
                c.decouvrir (caseCentrale.proprietaire()) ;
        }
    }
    
    
    // Rend l'ensemble du terrain visible par tous les joueurs en paramètre
    public void rendreVisiblePourTous (Collection<Joueur> joueurs)
    {
        for (int x = 0 ; x < this.largeur ; x++)
        for (int y = 0 ; y < this.hauteur ; y++)
        for (Joueur joueur : joueurs)
        {
            this.caseEn(x,y).decouvrir (joueur) ;
        }
    }
    
    
    // Met à jour le marquage des cases mortes : calcule quelles cases sont vivantes ou mortes et
    //   met à jour le amrquage des cases.
    public void mettreAJourCasesMortes ()
    {
        boolean[][] casesVivantes ;         // Cases vivantes ou non
        
        
        // Déterminer quelles cases sont vivantes
        casesVivantes = grilleCasesVivantes() ;
        
        // Mettre à jour le marquage des cases
        // TODO Ca en fait des notifications aux observateurs ; espérons qu'ils ne se mettent pas à jour à chaque fois (?)
        for (int x = 0 ; x < this.largeur ; x++)
        for (int y = 0 ; y < this.hauteur ; y++)
        {
            if (casesVivantes[x][y])
                this.caseEn(x,y).naitre() ;
            else
                this.caseEn(x,y).mourir() ;
        }
    }
    
    
    // Renvoie toutes les cases du terrain
    // PERF Surtout utile pour appliquer des filtres dessus. Attention, il se peut que les traitements
    //      basés là-dessus soient lents, à manier avec précaution. Commode pour écrire rapidement
    //      certains traitements
    //          => On pourrait lister les cases une fois pour toute et renvoyer la même liste à chaque fois
    //             (dangereux si la liste est modifiée dehors... mais on n'a pas fait ça pour les mers par
    //              exemple ?)
    //          => Si c'est trop violent d'un point de vue allocation mémoire, on peut aussi renvoyer un
    //             itérateur ou une liste encapsulant l'objet et capable de fournir un itérateur
    public List<Case> toutesLesCases ()
    {
        // Lister toutes les cases
        List<Case> cases = new ArrayList() ;
        for (int xCase = 0 ; xCase < this.largeur ; xCase++)
        for (int yCase = 0 ; yCase < this.hauteur ; yCase++)
        {
            cases.add (caseEn (xCase, yCase)) ;
        }
        
        // Renvoyer les cases
        return cases ;
    }
    
    
    // Renvoie les cases mortes du terrain
    // CODE Organiser ces méthodes (ordonner surtout)
    public List<Case> casesMortes ()
    {
        boolean[][] casesVivantes ;         // Cases vivantes ou non
        
        
        // Déterminer quelles cases sont vivantes
        casesVivantes = grilleCasesVivantes() ;
        
        // Lister les cases mortes
        List<Case> casesMortes = new ArrayList() ;
        for (int xCase = 0 ; xCase < this.largeur ; xCase++)
        for (int yCase = 0 ; yCase < this.hauteur ; yCase++)
        {
            if (! casesVivantes[xCase][yCase])
                casesMortes.add (caseEn (xCase, yCase)) ;           // TODO Utiliser this.cases[][] dans toute la méthode ; euh... il n'y a pas une contre-indication à cause des cases qui peuvent être remplacées (être masquées par d'autres ? non, peut-être que j'ai fait ça dans les cases finalement...)
        }
        
        // Renvoyer les cases mortes
        return casesMortes ;
    }
    
    
    // Détermine quelles cases sont vivantes et quelles cases sont mortes.
    // Les cases de mer sont considérées comme vivantes.
    // Renvoie une grille de booléens indiquant pour chaque case si elle est vivante ou morte
    // Les cases ne sont pas mises à jour.
    private boolean[][] grilleCasesVivantes ()
    {
        boolean[][] casesVivantes ;         // Cases vivantes ou non

        
        // Créer une grille servant à marquer les cases vivantes/mortes
        // (les cases de mer sont vivantes, les cases de villes aussi)
        casesVivantes = new boolean[this.largeur][this.hauteur] ;
        for (int xCase = 0 ; xCase < this.largeur ; xCase++)
        for (int yCase = 0 ; yCase < this.hauteur ; yCase++)
        {
            Case caseCourante = this.caseEn (xCase, yCase) ;
            casesVivantes[xCase][yCase] = (caseCourante.type() != TypeCase.CASE_TERRE) ;
        }
        
        // Parcourir la grille en propageant la vie
        // TODO Optimisation "minimale" : parcourir la grille dans les deux sens (voir si nécessaire)
        boolean modifEffectuee ;
        do
        {
            // Parcourir la grille
            modifEffectuee = false ;
            for (int xCase = 0 ; xCase < this.largeur ; xCase++)
            for (int yCase = 0 ; yCase < this.hauteur ; yCase++)
            {
                // Traiter les cases pas encore marquées comme vivantes
                if (! casesVivantes[xCase][yCase])
                {
                    Joueur proprietaireCaseTestee = caseEn(xCase,yCase).proprietaire() ;
                    int xDeb = Math.max (xCase - 1, 0) ;
                    int xFin = Math.min (xCase + 1, this.largeur - 1) ;
                    int yDeb = Math.max (yCase - 1, 0) ;
                    int yFin = Math.min (yCase + 1, this.hauteur - 1) ;

                    // Parcourir les cases voisines
                    for (int x = xDeb ; x <= xFin ; x++)
                    for (int y = yDeb ; y <= yFin ; y++)
                    {
                        // Une case est vivante si une de ses voisine est vivante et a le même propriétaire
                        if (casesVivantes[x][y] && proprietaireCaseTestee != null &&
                                                   proprietaireCaseTestee.equals (caseEn(x,y).proprietaire()))
                        {
                            casesVivantes[xCase][yCase] = true ;
                            modifEffectuee = true ;
                            // break ; // Plus compliqué : on a deux boucles imbriquées
                        }
                    }
                }
            }
        } while (modifEffectuee) ;

        // Renvoyer la grille
        return casesVivantes ;
    }
    
    
    // Renvoie la grille des distances entre une case données et les autres cases.
    // La première dimension représente la largeur.
    // La valeur dans une case de la grille indique combien de cases il faut parcourir pour arriver dans
    //   cette case depuis la case de départ (case de départ exclue).
    // null signifie que la case ne peut pas être atteinte.
    // TODO Prendre en compte les ports pour la traversée des mers.
    // TODO Bien tester
    // TODO Remplacer l'utilisation de cette méthode par celles de CourtChemin
    public Integer[][] grilleDistancesDepuis (Case caseDepart)
    {
        Integer[][] grille = new Integer[this.largeur][this.hauteur] ;
        
        
        // Initialiser le tableau
        for (int x = 0 ; x < this.largeur ; x++)
        for (int y = 0 ; y < this.hauteur ; y++)
            grille[x][y] = null ;
        
        // Inscrire 0 dans la case centrale
        grille[caseDepart.x()][caseDepart.y()] = 0 ;
     
        // Propager les distances minimales tant que c'est possible
        boolean propagation ;
        do
        {
            propagation = false ;
            
            // Parcourir la grille
            // (premier sens)
            for (int x = 0 ; x < this.largeur ; x++)
            for (int y = 0 ; y < this.hauteur ; y++)
                propagation |= calculerCaseDistanceDepuis (x, y, grille) ;
            // (deuxème sens [perf])
            for (int x = this.largeur - 1 ; x >= 0 ; x--)
            for (int y = this.hauteur - 1 ; y >= 0 ; y--)
                propagation |= calculerCaseDistanceDepuis (x, y, grille) ;
            
        } while (propagation) ;
        
        
        // Renvoyer la grille
        return grille ;
    }
    // Calcule la valeur d'une case à partir de celle de ses voisines.
    // La case de la grille et modifiée si une plus courte distance a été trouvée pour atteindre cette
    //   case.
    // Renvoie vrai si la case a été modifiée.
    private boolean calculerCaseDistanceDepuis (int x, int y, Integer[][] grille)
    {
        boolean modification = false ;
        
        // Ne pas traiter les cases de mer
        Case caseConsideree = this.caseEn (x, y) ;
        if (caseConsideree.type() != TypeCase.CASE_MER)
        {
            // Rechercher une plus courte distance pour arriver à ce point
            List<Case> casesVoisines = casesVoisines (caseConsideree) ;
            for (Case c : casesVoisines)
            {
                Integer valCaseCourante = grille[x][y] ;
                Integer valCaseVoisine  = grille[c.x()][c.y()] ;
                if (valCaseVoisine != null)
                {
                    if (valCaseCourante == null || valCaseCourante > valCaseVoisine + 1)
                    {
                        grille[x][y] = valCaseVoisine + 1 ;
                        modification = true ;
                    }
                }
            }
        }
        
        // Indiquer si la case a été modifiée
        return modification ;
    }
    
    
    // Renvoie les cases vivantes possédées par un joueur donné et sur lesquelles se trouvent une troupe.
    // Renvoie une liste vide si aucune case trouvée.
    public List<Case> casesAvecTroupeDisponible (Joueur proprietaire)
    {
        List<Case> casesSelectionnees = new ArrayList() ;
        
        // Chercher les cases
        for (int xCase = 0 ; xCase < this.largeur ; xCase++)
        for (int yCase = 0 ; yCase < this.hauteur ; yCase++)
        {
            Case c = this.caseEn (xCase, yCase) ;
            if (c.estVivante() && c.troupePresente() && proprietaire.equals (c.proprietaire()))
                casesSelectionnees.add (c) ;
        }
        
        // Renvoyer les cases trouvées
        return casesSelectionnees ;
    }
    
    
    // Renvoie les cases de terre vivantes possédées par un joueur donné et sur lesquelles ne se trouvent
    //   pas de troupe. Les villes ne sont pas renvoyées.
    // Renvoie une liste vide si aucune case trouvée.
    public List<Case> casesSansTroupeDisponible (Joueur proprietaire)
    {
        List<Case> casesSelectionnees = new ArrayList() ;
        
        // Chercher les cases
        for (int xCase = 0 ; xCase < this.largeur ; xCase++)
        for (int yCase = 0 ; yCase < this.hauteur ; yCase++)
        {
            Case c = this.caseEn (xCase, yCase) ;
            if (c.estVivante() && ! c.troupePresente() && proprietaire.equals (c.proprietaire()) && c.type() == TypeCase.CASE_TERRE)
                casesSelectionnees.add (c) ;
        }
        
        // Renvoyer les cases trouvées
        return casesSelectionnees ;
    }
    
    
    // Indique le nombre de villes présentes sur le terrain
    public int nbVilles ()
    {
        int nbVilles ;
        
        
        // Compter les villes
        nbVilles = 0 ;
        for (int x = 0 ; x < this.largeur ; x++)
        for (int y = 0 ; y < this.hauteur ; y++)
        {
            Case c = this.caseEn (x, y) ;
            if (c.type() == TypeCase.CASE_VILLE)
                nbVilles++ ;
        }
        
        // Renvoyer le nombre de villes
        return nbVilles ;
    }
    
    
    // Indique le nombre de cases de terres (sans ville) contôlées par le joueur donné
    public int nbTerresControleesPar (Joueur joueur)
    {
        int nbTerres ;
        
        
        // Compter les cases du joueur
        nbTerres = 0 ;
        for (int x = 0 ; x < this.largeur ; x++)
        for (int y = 0 ; y < this.hauteur ; y++)
        {
            Case c = this.caseEn (x, y) ;
            if (c.type() == TypeCase.CASE_TERRE && joueur.equals (c.proprietaire()))
                nbTerres++ ;
        }
        
        // Renvoyer le nombre de cases de terre du joueur
        return nbTerres ;
    }
    
    
    // Indique le nombre de cases de terres (sans ville) vivantes contôlées par le joueur donné
    public int nbTerresVivantesControleesPar (Joueur joueur)
    {
        int nbTerres ;
        
        
        // Compter les cases du joueur
        nbTerres = 0 ;
        for (int x = 0 ; x < this.largeur ; x++)
        for (int y = 0 ; y < this.hauteur ; y++)
        {
            Case c = this.caseEn (x, y) ;
            if (c.type() == TypeCase.CASE_TERRE && joueur.equals (c.proprietaire()) && c.estVivante())
                nbTerres++ ;
        }
        
        // Renvoyer le nombre de cases de terre du joueur
        return nbTerres ;
    }
    
    
    // Indique le nombre de villes contôlées par le joueur donné
    public int nbVillesControleesPar (Joueur joueur)
    {
        int nbVilles ;
        
        
        // Compter les villes du joueur
        nbVilles = 0 ;
        for (int x = 0 ; x < this.largeur ; x++)
        for (int y = 0 ; y < this.hauteur ; y++)
        {
            Case c = this.caseEn (x, y) ;
            if (c.type() == TypeCase.CASE_VILLE && joueur.equals (c.proprietaire()))
                nbVilles++ ;
        }
        
        // Renvoyer le nombre de villes du joueur
        return nbVilles ;
    }
    
    
    // Indique si l'une des cases voisines de celle dont les coordonnées sont passées en paramètre
    //   appartient au joueur donné. La case elle-même n'est pas prise en compte.
    public boolean existeVoisineVivanteAppartenantA (int xCase, int yCase, Joueur joueur)
    {
        // Vérifier les cases
        for (Case caseVoisine : casesVoisinesVivantes (xCase, yCase))
        {
            if (joueur.equals (caseVoisine.proprietaire()))
                return true ;
        }
        
        // Joueur non trouvé dans les cases autour
        return false ;
    }
    
    
    // Indique si l'une des cases voisines de celle dont les coordonnées sont passées en paramètre
    //   est non-découverte par le joueur en paramètre. La case elle-même n'est pas prise en compte.
    public boolean existeVoisineNonDecouvertePar (int xCase, int yCase, Joueur joueur)
    {
        // Vérifier les cases
        for (Case caseVoisine : casesVoisinesVivantes (xCase, yCase))
        {
            if (! caseVoisine.decouverte (joueur))
                return true ;
        }
        
        // Aucune case trouvée sur ce critère
        return false ;
    }
    
    
    // Renvoie le nombre de troupes d'un joueur donné sur les cases voisines de celle dont les
    //   coordonnées sont passées en paramètre
    public int nbTroupesVoisinesVivantesAppartenantA (int xCase, int yCase, Joueur joueur)
    {
        // Compter les troupes
        int nbTroupes = 0 ;
        for (Case caseVoisine : casesVoisinesVivantes (xCase, yCase))
        {
            if (caseVoisine.troupePresente() && joueur.equals (caseVoisine.proprietaire()))
                nbTroupes++ ;
        }
        
        // Renvoyer le nombre de troupes
        return nbTroupes ;
    }
    
    
    // Renvoie le nombre de villes d'un joueur donné sur les cases voisines de celle dont les
    //   coordonnées sont passées en paramètre
    public int nbVillesVoisinesAppartenantA (int xCase, int yCase, Joueur joueur)
    {
        // Compter les villes
        int nbVilles = 0 ;
        for (Case caseVoisine : casesVoisines (xCase, yCase))
        {
            if (joueur.equals (caseVoisine.proprietaire()) && caseVoisine.type() == TypeCase.CASE_VILLE)
                nbVilles++ ;
        }
        
        // Renvoyer le nombre de villes
        return nbVilles ;
    }
    
    
    // Renvoie la liste des cases voisines de la case dont les coordonnées sont indiquées en paramètre.
    // Si le type n'est pas null, filtre sur le type de la case
    // La case centrale n'est pas renvoyée
    public List<Case> casesVoisines (Case caseCentrale)
    {
        return casesVoisines (caseCentrale.x(), caseCentrale.y(), null) ;
    }
    public List<Case> casesVoisines (int xCase, int yCase)
    {
        return casesVoisines (xCase, yCase, null) ;
    }
    public List<Case> casesVoisines (Case caseCentrale, TypeCase type)
    {
        return casesVoisines (caseCentrale.x(), caseCentrale.y(), type) ;
    }
    public List<Case> casesVoisines (int xCase, int yCase, TypeCase type)
    {
        int xDeb = Math.max (xCase - 1, 0) ;
        int xFin = Math.min (xCase + 1, this.largeur - 1) ;
        int yDeb = Math.max (yCase - 1, 0) ;
        int yFin = Math.min (yCase + 1, this.hauteur - 1) ;
        
        // Lister les cases
        List<Case> casesVoisines = new ArrayList() ;
        for (int x = xDeb ; x <= xFin ; x++)
        for (int y = yDeb ; y <= yFin ; y++)
        {
            if (! (x == xCase && y == yCase) && (type == null || type.equals (caseEn(x,y).type())))
                casesVoisines.add (caseEn (x, y)) ;
        }
        
        // Renvoyer les cases
        return casesVoisines ;
    }
    
    
    // Renvoie les cases voisine vivantes de la case dont les coordonnées sont indiquées en paramètre.
    // La case centrale n'est pas renvoyée
    public List<Case> casesVoisinesVivantes (int xCase, int yCase)
    {
        // Récupérer et filtrer la liste des cases voisines
        List<Case>     casesVoisines   = casesVoisines (xCase, yCase) ;
        Iterator<Case> itCasesVoisines = casesVoisines.iterator() ;
        while (itCasesVoisines.hasNext())
        {
            if (itCasesVoisines.next().estMorte())
                itCasesVoisines.remove() ;
        }

        // Renvoyer les cases filtrées
        return casesVoisines ;
    }
    
    
    // Renvoie les propriétaires des cases voisine vivantes de la case dont les coordonnées sont indiquées
    //   en paramètre
    // Le propriétaire de la case centrale n'est pas renvoyée, si un joueur possède plusieurs cases il
    //   est renvoyé plusieurs fois, si une case n'a pas de propriétaire rien n'est ajouté dans la liste.
    public List<Joueur> proprietairesCasesVoisinesVivantes (int xCase, int yCase)
    {
        // Récupérer les propriétaires voisins
        List<Joueur> proprietaires = new ArrayList() ;
        for (Case caseVoisine : casesVoisinesVivantes (xCase, yCase))
        {
            if (caseVoisine.proprietaire() != null)
                proprietaires.add (caseVoisine.proprietaire()) ;
        }
        
        // Renvoyer la liste des propriétaires
        return proprietaires ;
    }
    

    // Renvoie les cases environnantes à partir des coordonnées d'une case.
    // Les cases environnantes incluent la case aux coordonnées indiquées et ses voisines
    public List<Case> casesEnvironnantes (int xCase, int yCase)
    {
        List<Case> casesEnvironnantes = casesVoisines (xCase, yCase) ;
        casesEnvironnantes.add (this.caseEn (xCase, yCase)) ;
        return casesEnvironnantes ;
    }
    
    
    // Indique combien de cases non-découverte par le joueur donné contient l'environnement aux coordonnées
    //   en paramètre.
    // Les cases observées incluent la case aux coordonnées courantes et ses voisines.
    // Le résultat devrait être compris entre 0 et 9
    public int nbCasesEnvironnantesNonDecouvertesPar (int xCase, int yCase, Joueur joueur)
    {
        // Compter les cases
        int cptCases = 0 ;
        for (Case c : casesEnvironnantes (xCase, yCase))
        {
            if (! c.decouverte (joueur))
                cptCases++ ;
        }
        
        // Renvoyer le résultat
        return cptCases ;
    }
    
    
    // Renvoie les cases accessible depuis une case donnée.
    // Les cases incluent les cases voisines et si la case de départ est un port, les cases de côte sur
    //   lesquelles on peut débarquer depuis ce port.
    // TODO A bien tester
    public List<Case> casesAccessiblesDepuis (Case caseOrigine, boolean invasionsNavalesPermises)
    {
        Mer        merVoisine ;         // Mer voisine de la cas d'origine
        List<Case> casesAccessibles ;   // Cases accessibles à renvoyer
        

        // Cases voisines
        casesAccessibles = nonDeType (CASE_MER, casesVoisines (caseOrigine)) ;
        
        // Cases accessibles par débarquement
        // TODO Des fonctions de plus haut niveau pour les informations concernant les ports
        if (invasionsNavalesPermises)
        {
            merVoisine = merBordant (caseOrigine.x(), caseOrigine.y()) ;
            if (merVoisine != null && caseOrigine.type() == CASE_VILLE)
            {
                casesAccessibles.addAll (merVoisine.cotes()) ;
                casesAccessibles.remove (caseOrigine) ;
            }
        }

        // Renvoyer le résultat
        return casesAccessibles ;
    }
    
    
    // Renvoie la mer correspondant à la case de côte (terre) de coordonnées données
    // Si ce n'est pas une case de côte, renvoie null
    public Mer merBordant (int x, int y)
    {
        return this.cotes.get (this.caseEn (x, y)) ;
    }

    
    
    // **************************************************************************************************
    //                                      Filtres sur les cases
    // **************************************************************************************************
    // TODO Utiliser les filtres pour implémenter plus simplement les autres méthodes de cette classe
    //      et aussi les méthodes externes qui filtrent des cases.

    // Renvoie dans une liste les cases en paramètre appartenant au joueur donné.
    public static List<Case> appartenantA (Joueur proprietaire, Collection<Case> cases)
    {
        List<Case> casesSelectionnees = new ArrayList() ;
        for (Case c : cases)
        {
            if (proprietaire.equals (c.proprietaire()))
                casesSelectionnees.add (c) ;
        }
        return casesSelectionnees ;
    }
    // Renvoie dans une liste les cases en paramètre n'appartenant pas au joueur donné.
    public static List<Case> nAppartenantPasA (Joueur proprietaire, Collection<Case> cases)
    {
        List<Case> casesSelectionnees = new ArrayList() ;
        for (Case c : cases)
        {
            if (! proprietaire.equals (c.proprietaire()))
                casesSelectionnees.add (c) ;
        }
        return casesSelectionnees ;
    }
    // Renvoie dans une liste les cases en paramètre de type donné.
    public static List<Case> deType (TypeCase type, Collection<Case> cases)
    {
        List<Case> casesSelectionnees = new ArrayList() ;
        for (Case c : cases)
        {
            if (type.equals (c.type()))
                casesSelectionnees.add (c) ;
        }
        return casesSelectionnees ;
    }
    // Renvoie dans une liste les cases en paramètre qui ne sont pas de type donné.
    public static List<Case> nonDeType (TypeCase type, Collection<Case> cases)
    {
        List<Case> casesSelectionnees = new ArrayList() ;
        for (Case c : cases)
        {
            if (! type.equals (c.type()))
                casesSelectionnees.add (c) ;
        }
        return casesSelectionnees ;
    }
    // Renvoie dans une liste les cases en paramètre qui sont vivantes.
    public static List<Case> vivantes (Collection<Case> cases)
    {
        List<Case> casesSelectionnees = new ArrayList() ;
        for (Case c : cases)
        {
            if (c.estVivante())
                casesSelectionnees.add (c) ;
        }
        return casesSelectionnees ;
    }
    // Renvoie dans une liste les cases en paramètre qui sont mortes.
    public static List<Case> mortes (Collection<Case> cases)
    {
        List<Case> casesSelectionnees = new ArrayList() ;
        for (Case c : cases)
        {
            if (! c.estVivante())
                casesSelectionnees.add (c) ;
        }
        return casesSelectionnees ;
    }
    // Renvoie dans une liste les cases en paramètre sur lesquelles se trouve une troupe.
    public static List<Case> troupePresente (Collection<Case> cases)
    {
        List<Case> casesSelectionnees = new ArrayList() ;
        for (Case c : cases)
        {
            if (c.troupePresente())
                casesSelectionnees.add (c) ;
        }
        return casesSelectionnees ;
    }
    // Renvoie dans une liste les cases en paramètre sur lesquelles ne se trouve pas de troupe.
    public static List<Case> nonTroupePresente (Collection<Case> cases)
    {
        List<Case> casesSelectionnees = new ArrayList() ;
        for (Case c : cases)
        {
            if (! c.troupePresente())
                casesSelectionnees.add (c) ;
        }
        return casesSelectionnees ;
    }
    // Renvoie dans une liste les cases en paramètre sur lesquelles on peut aller
    // CODE Faire quelque part une liste unique des types de cases sur lesquelles on peut aller
    public static List<Case> casesUtilisables (Collection<Case> cases)
    {
        return nonDeType (CASE_MER, cases) ;
    }
    
}
